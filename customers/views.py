from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework import status
from .models import *
from .serializers import *
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.db.models import Count, Q, Sum
from django.utils import timezone
from django.shortcuts import get_object_or_404
from datetime import datetime

#Estadisticas
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def estadisticas(request):
    estadisticas = Siembra.objects.values('Id_hortaliza__Nombre').annotate(total=Count('Id_hortaliza')).order_by('-total')
    return Response(estadisticas, status=status.HTTP_200_OK)

#HuertoDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def inventario_huerto(request, pk):
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    inventario = Siembra.objects.filter(Id_huerto=huerto).values('Id_hortaliza__Nombre').annotate(total=Count('Id_hortaliza__Nombre'))

    return Response({'inventario': list(inventario)}, status=status.HTTP_200_OK)

#MisHuertos
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def check_huerto_name(request):
    nombre = request.data.get('nombre')
    user = request.user
    if Huerto.objects.filter(nombre=nombre, id_user=user).exists():
        return Response({"message": "Huerto ya existe!"}, status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_200_OK)

#MisHuertos
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def mis_huertos(request):
    huertos = Huerto.objects.filter(id_user=request.user).order_by('-fecha_creacion')
    serializer = HuertoSerializer(huertos, many=True)
    
    return Response(serializer.data, status=status.HTTP_200_OK)

#MisHuertos
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def crear_huerto(request):
    serializer = HuertoSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(id_user=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#MisHuertos   //  #HuertoDetalle
@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def huertos_detail(request, pk):
    try:
        huerto = Huerto.objects.get(id=pk)
    except Huerto.DoesNotExist:
        return Response({"noHuerto": "Este huerto no existe."}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = HuertoSerializer(huerto,context={'request': request})
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = HuertoSerializer(huerto, data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        huerto.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#Register
@api_view(['POST'])
@authentication_classes([])  # Deshabilita la autenticación
@permission_classes([AllowAny])  # Permite a cualquiera acceder
def register(request):
    serializer = UserSerializer(data=request.data)
    username = request.data.get('username')
    if User.objects.filter(username=username).exists():
        return Response({"username": "Este usuario ya está en uso."}, status=status.HTTP_400_BAD_REQUEST)
    if serializer.is_valid():
        email = serializer.validated_data['email']
        if User.objects.filter(email=email).exists():
            return Response({"email": "Este correo electrónico ya está en uso."}, status=status.HTTP_400_BAD_REQUEST)
        password = make_password(serializer.validated_data['password'], hasher='pbkdf2_sha256')
        serializer.validated_data['password'] = password
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Login
@api_view(['POST'])
@authentication_classes([])  # Deshabilita la autenticación
@permission_classes([AllowAny])  # Permite a cualquiera acceder
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    
    if not User.objects.filter(username=username).exists():
        return Response({'error': 'El usuario introducido no existe!'}, status=status.HTTP_400_BAD_REQUEST)
    
    user = authenticate(username=username, password=password)
    
    if user is None:
        return Response({'error': 'Credenciales de inicio de sesión no válidas'}, status=status.HTTP_400_BAD_REQUEST)
    
    token, _ = Token.objects.get_or_create(user=user)
    
    return Response({'token': token.key, 'user_id': user.id}, status=status.HTTP_200_OK)

#Perfil
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def change_password(request):
    user = request.user
    old_password = request.data.get('old_password')
    new_password = request.data.get('new_password')
    if not user.check_password(old_password):
        return Response({"old_password": "La contraseña antigua no es correcta."}, status=status.HTTP_400_BAD_REQUEST)
    if old_password == new_password:
        return Response({"new_password": "La nueva contraseña debe ser diferente a la antigua."}, status=status.HTTP_400_BAD_REQUEST)
    new_password = make_password(new_password, hasher='pbkdf2_sha256')
    user.password = new_password
    user.save()

    return Response({"success": "Contraseña actualizada con éxito."}, status=status.HTTP_200_OK)

#Perfil
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_user_data(request):
    user = request.user
    data = {
        'username': user.username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
    }
    return Response(data)

#Perfil
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def update_user_data(request):
    user = request.user
    data = request.data
    user.first_name = data.get('first_name', user.first_name)
    user.last_name = data.get('last_name', user.last_name)
    user.email = data.get('email', user.email)
    user.save()

    return Response({'message': 'Datos actualizados con éxito'})

#Dashboard  //  #Header
@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def user_detail(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user, context={'request': request})
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UserSerializer(user, data=request.data, context={'request': request})
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#HortalizasFamilia
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_por_familia(request, familia_id):
    familia = get_object_or_404(Familias, id=familia_id)
    hortalizas = Hortalizas.objects.filter(Familia=familia_id).order_by('Nombre')
    serializer = HortalizasSerializer(hortalizas, many=True)
    return Response(serializer.data)

#MisHortalizas   //  #HuertoDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def mis_hortalizas(request):
    hortalizas = Hortalizas.objects.filter(Id_user=request.user).order_by('Nombre')
    serializer = HortalizasSerializer(hortalizas, many=True)
    return Response(serializer.data)

#HortalizasSistema   //  #HuertoDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_sistema(request):
    hortalizas = Hortalizas.objects.filter(Id_user__isnull=True).order_by('Nombre')
    serializer = HortalizasSerializer(hortalizas, many=True)
    return Response(serializer.data)

#CrearHortaliza
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_list(request):
    if request.method == 'GET':
        hortalizas = Hortalizas.objects.filter(Q(Id_user__isnull=True) | Q(Id_user=request.user))
        serializer = HortalizasSerializer(hortalizas, context={'request': request}, many=True)
        return Response({'data': serializer.data, 'count': len(hortalizas)})

    elif request.method == 'POST':
        serializer = HortalizasSerializer(data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#HortalizaDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_detail_v2(request, pk):
    hortaliza = get_object_or_404(Hortalizas, pk=pk)
    serializer = HortalizasSerializer(hortaliza, context={'request': request})
    return Response(serializer.data)

#CrearHortaliza  //  #HortalizasFamilia
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def familias_list(request):
    if request.method == 'GET':
        familias = Familias.objects.all()
        familias = sorted(familias, key=lambda x: x.nombre)
        serializer = FamiliasSerializer(familias, many=True, context={'request': request})
        return Response({'data': serializer.data, 'count': len(serializer.data)})

    elif request.method == 'POST':
        serializer = FamiliasSerializer(data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#CrearHortaliza
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def tipo_riego_list(request):
    if request.method == 'GET':
        tipo_riego = Tipo_riego.objects.all()
        serializer = TipoRiegoSerializer(tipo_riego, context={'request': request}, many=True)
        return Response({'data': serializer.data})

    elif request.method == 'POST':
        serializer = TipoRiegoSerializer(data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#GastosHuerto
@api_view(['PUT'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def update_gasto(request, pk):
    try:
        gasto = Gastos.objects.get(pk=pk)
    except Gastos.DoesNotExist:
        return Response({"noGasto": "Este gasto no existe."}, status=status.HTTP_404_NOT_FOUND)

    new_nota = request.data.get('nota')
    new_precio = request.data.get('precio')
    new_fecha_str = request.data.get('fecha')
    new_fecha = datetime.strptime(new_fecha_str, '%Y-%m-%dT%H:%M:%S.%fZ')
    gasto.nota = new_nota
    gasto.precio = new_precio
    gasto.fecha = new_fecha
    gasto.save()

    return Response({"success": "Gasto actualizado con éxito."}, status=status.HTTP_200_OK)

#GastosHuerto
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def gastos_huerto(request, pk):
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        gastos = Gastos.objects.filter(id_huerto=huerto)
        serializer = GastosSerializer(gastos, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = GastosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(id_huerto=huerto)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#GastosHuerto
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def gasto_create(request, pk):
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['id_huerto'] = huerto.id
    serializer = GastosSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#GastosHuerto
@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def gasto_delete(request, pk):
    try:
        gasto = Gastos.objects.get(pk=pk)
    except Gastos.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    gasto.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#TareasDetalle
@api_view(['PUT'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def update_tarea(request, pk):
    try:
        tarea = Tareas.objects.get(pk=pk)
    except Tareas.DoesNotExist:
        return Response({"noTarea": "Esta tarea no existe."}, status=status.HTTP_404_NOT_FOUND)

    new_nota = request.data.get('nota')
    new_fecha_str = request.data.get('fecha')
    new_estado_id = request.data.get('id_estado')
    new_fecha = datetime.strptime(new_fecha_str, '%Y-%m-%dT%H:%M:%S.%fZ')
    tarea.nota = new_nota
    tarea.fecha = new_fecha
    tarea.id_estado_id = new_estado_id
    tarea.save()

    return Response({"success": "Tarea actualizada con éxito."}, status=status.HTTP_200_OK)

#TareasDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def estado_tarea_list(request):
    estado_tarea = Estado_tarea.objects.all()
    serializer = EstadoTareaSerializer(estado_tarea, many=True)
    return Response(serializer.data)

#TareasDetalle
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def tareas_huerto(request, pk):
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        tareas = Tareas.objects.filter(id_huerto=huerto)
        serializer = TareasSerializer(tareas, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = TareasSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(id_huerto=huerto)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#TareasDetalle
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def tarea_create(request, pk):
    try:
        huerto = Huerto.objects.get(pk=pk)
    except Huerto.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['id_huerto'] = huerto.id
    serializer = TareasSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#TareasDetalle
@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def tarea_delete(request, pk):
    try:
        tarea = Tareas.objects.get(pk=pk)
    except Tareas.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    tarea.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#HuertoDetalle
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def create_plaga(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
    except (Huerto.DoesNotExist, Celda.DoesNotExist):
        return Response({"error": "El huerto o la celda no existen."}, status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['id_huerto'] = huerto.id
    data['id_celda'] = celda.id
    data['fecha'] = timezone.now()
    serializer = PlagasSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#InfoModal
@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_plaga(request, pk):
    try:
        plaga = Plagas.objects.get(pk=pk)
    except Plagas.DoesNotExist:
        return Response({"error": "La plaga no existe."}, status=status.HTTP_404_NOT_FOUND)

    plaga.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#InfoModal   //  #HuertoDetalle
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def plagas_list(request):
    id_huerto = request.GET.get('id_huerto', None)
    id_celda = request.GET.get('id_celda', None)
    
    if id_huerto is not None and id_celda is not None:
        plagas = Plagas.objects.filter(id_huerto=id_huerto, id_celda=id_celda)
    else:
        plagas = Plagas.objects.all()

    if request.method == 'GET':
        serializer = PlagasSerializer(plagas, context={'request': request}, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = PlagasSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#HuertoDetalle
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def create_riego(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
    except (Huerto.DoesNotExist, Celda.DoesNotExist):
        return Response({"error": "El huerto o la celda no existen."}, status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['id_huerto'] = huerto.id
    data['id_celda'] = celda.id
    data['fecha'] = timezone.now()
    serializer = RiegosSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#InfoModal
@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_riego(request, pk):
    try:
        riego = Riegos.objects.get(pk=pk)
    except Riegos.DoesNotExist:
        return Response({"error": "El riego no existe."}, status=status.HTTP_404_NOT_FOUND)

    riego.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#InfoModal   //  #HuertoDetalle
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def riegos_list(request):
    id_huerto = request.GET.get('id_huerto', None)
    id_celda = request.GET.get('id_celda', None)
    
    if id_huerto is not None and id_celda is not None:
        riegos = Riegos.objects.filter(id_huerto=id_huerto, id_celda=id_celda)
    else:
        riegos = Riegos.objects.all()

    if request.method == 'GET':
        serializer = RiegosSerializer(riegos, context={'request': request}, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = RiegosSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#HortalizaDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_asociadas(request, Id_hortaliza):
    aso_ok_list = Aso_ok.objects.filter(Id_hortaliza=Id_hortaliza)
    
    hortalizas_asociadas = []
    for aso_ok in aso_ok_list:
        hortalizas_asociadas.append(aso_ok.Id_horta_aso)

    hortalizas_asociadas_serializer = HortalizasSerializer(hortalizas_asociadas, many=True)
    return Response({'data': hortalizas_asociadas_serializer.data, 'count': len(hortalizas_asociadas_serializer.data)})

#HortalizaDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def hortalizas_no_asociadas(request, Id_hortaliza):
    aso_ko_list = Aso_ko.objects.filter(Id_hortaliza=Id_hortaliza)
    
    hortalizas_no_asociadas = []
    for aso_ko in aso_ko_list:
        hortalizas_no_asociadas.append(aso_ko.Id_horta_aso)

    hortalizas_no_asociadas_serializer = HortalizasSerializer(hortalizas_no_asociadas, many=True)
    return Response({'data': hortalizas_no_asociadas_serializer.data, 'count': len(hortalizas_no_asociadas_serializer.data)})

#HortalizaDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def cal_siembra_hortaliza(request, Id_hortaliza):
    try:
        calendario = Cal_siembra.objects.filter(Id_hortaliza=Id_hortaliza)
        meses = calendario.values_list('Id_mes__nombre', flat=True)

        return Response({'data': list(meses)})
    
    except Hortalizas.DoesNotExist:
        return Response({'error': 'La hortaliza no existe'}, status=status.HTTP_404_NOT_FOUND)
    
    except Cal_siembra.DoesNotExist:
        return Response({'error': 'No hay datos de siembra para la hortaliza'}, status=status.HTTP_404_NOT_FOUND)
    
    except Exception as e:
        return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

#Calendario // #CalMeses
@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def cal_siembra_list(request):
    if request.method == 'GET':
        Id_hortaliza = request.GET.get('Id_hortaliza', None)
        if Id_hortaliza is not None:
            cal_siembra = Cal_siembra.objects.filter(Id_hortaliza=Id_hortaliza)
            
        else:
            cal_siembra = Cal_siembra.objects.all().order_by('Id_hortaliza__Nombre')

        serializer = CalSiembraSerializer(cal_siembra, context={'request': request}, many=True)
        
        return Response({'data': serializer.data})
        
    elif request.method == 'POST':
        serializer = CalSiembraSerializer(data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#HuertoDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_siembras(request, huerto_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        siembras = Siembra.objects.filter(Id_huerto=huerto)
    except Huerto.DoesNotExist:
        return Response({"error": "El huerto no existe."}, status=status.HTTP_404_NOT_FOUND)

    serializer = SiembraSerializer(siembras, context={'request': request}, many=True)
    return Response(serializer.data)

#HuertoDetalle
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_siembra(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
        siembra = Siembra.objects.get(Id_huerto=huerto, Id_celda=celda)
    except (Huerto.DoesNotExist, Celda.DoesNotExist, Siembra.DoesNotExist):
        return Response({"error": "El huerto, la celda o la siembra no existen."}, status=status.HTTP_404_NOT_FOUND)

    serializer = SiembraSerializer(siembra, context={'request': request})
    return Response({
        "siembra": serializer.data,
        "Fecha_cos_real": siembra.Fecha_cos_real.strftime("%Y-%m-%d %H:%M:%S") if siembra.Fecha_cos_real else None,
    })

#HuertoDetalle
@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def create_siembra(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
        if Siembra.objects.filter(Id_huerto=huerto, Id_celda=celda).exists():
            return Response({"error": "Ya existe una siembra en esta celda."}, status=status.HTTP_400_BAD_REQUEST)
    except (Huerto.DoesNotExist, Celda.DoesNotExist):
        return Response({"error": "El huerto o la celda no existen."}, status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['Id_huerto'] = huerto.id
    data['Id_celda'] = celda.id
    data['Fecha_siembra'] = timezone.now()
    serializer = SiembraSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#HuertoDetalle
@api_view(['DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def delete_siembra(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
        siembra = Siembra.objects.get(Id_huerto=huerto, Id_celda=celda)
    except (Huerto.DoesNotExist, Celda.DoesNotExist, Siembra.DoesNotExist):
        return Response({"error": "El huerto, la celda o la siembra no existen."}, status=status.HTTP_404_NOT_FOUND)

    siembra.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#HuertoDetalle
@api_view(['DELETE', 'PUT'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def manage_siembra(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
        siembra = Siembra.objects.get(Id_huerto=huerto, Id_celda=celda)
    except (Huerto.DoesNotExist, Celda.DoesNotExist, Siembra.DoesNotExist):
        return Response({"error": "El huerto, la celda o la siembra no existen."}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'DELETE':
        siembra.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    elif request.method == 'PUT':
        Fecha_cos_real = request.data.get('Fecha_cos_real')
        if Fecha_cos_real:
            siembra.Fecha_cos_real = Fecha_cos_real
            siembra.save()
            return Response({"message": "Fecha_cos_real actualizada correctamente."}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "El campo fecha_cos_real es requerido para la actualización."}, status=status.HTTP_400_BAD_REQUEST)

#HuertoDetalle
@api_view(['PUT'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def update_siembra(request, huerto_id, celda_id):
    try:
        huerto = Huerto.objects.get(pk=huerto_id)
        celda = Celda.objects.get(pk=celda_id)
        siembra = Siembra.objects.get(Id_huerto=huerto, Id_celda=celda)
    except (Huerto.DoesNotExist, Celda.DoesNotExist, Siembra.DoesNotExist):
        return Response({"error": "El huerto, la celda o la siembra no existen."}, status=status.HTTP_404_NOT_FOUND)

    data = request.data.copy()
    data['Fecha_siembra'] = siembra.Fecha_siembra
    data['Id_huerto'] = huerto_id
    data['Id_celda'] = celda_id
    serializer = SiembraSerializer(siembra, data=data, context={'request': request})
        
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
        
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
