from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Familias(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=40)

    def __str__(self):
        return self.nombre

class Tipo_riego(models.Model):
    id = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=40)

    def __str__(self):
        return self.tipo

class Mes(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=40)

    def __str__(self):
        return self.nombre

class Huerto(models.Model):
    id = models.AutoField(primary_key=True)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    nombre = models.CharField(max_length=60)

    def __str__(self):
        return self.nombre

class Celda(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    fila = models.IntegerField()
    columna = models.IntegerField()

    def __str__(self):
        return self.id

class Gastos(models.Model):
    id = models.AutoField(primary_key=True)
    id_huerto = models.ForeignKey(Huerto, on_delete=models.CASCADE)
    fecha = models.DateTimeField()
    nota = models.CharField(max_length=255)
    precio = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.nota

class Estado_tarea(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.id

class Tareas(models.Model):
    id = models.AutoField(primary_key=True)
    id_huerto = models.ForeignKey(Huerto, on_delete=models.CASCADE)
    id_estado = models.ForeignKey(Estado_tarea, on_delete=models.CASCADE)
    fecha = models.DateTimeField()
    nota = models.CharField(max_length=255)

    def __str__(self):
        return self.nota
    
class Plagas(models.Model):
    id_huerto = models.ForeignKey(Huerto, on_delete=models.CASCADE)
    id_celda = models.ForeignKey(Celda, on_delete=models.CASCADE)
    fecha = models.DateTimeField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_huerto', 'id_celda', 'fecha'], name='unique_constraint')
        ]

class Riegos(models.Model):
    id_huerto = models.ForeignKey(Huerto, on_delete=models.CASCADE)
    id_celda = models.ForeignKey(Celda, on_delete=models.CASCADE)
    fecha = models.DateTimeField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['id_huerto', 'id_celda', 'fecha'], name='unique_constraint2')
        ]

class Hortalizas(models.Model):
    Id = models.AutoField(primary_key=True)
    Nombre = models.CharField(max_length=40)
    Tipo_riego = models.ForeignKey(Tipo_riego, on_delete=models.SET_NULL, null=True, blank=True)
    Familia = models.ForeignKey(Familias, on_delete=models.SET_NULL, null=True, blank=True)
    Id_user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    Id_horta_madre = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    Fecha_creacion = models.DateTimeField(null=True, blank=True)
    Dis_filas = models.IntegerField(null=True, blank=True)
    Dis_plantas = models.IntegerField(null=True, blank=True)
    Dias_a_cosecha = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.Nombre

class Aso_ok(models.Model):
    Id_hortaliza = models.ForeignKey(Hortalizas, on_delete=models.CASCADE, related_name='asociadas')
    Id_horta_aso = models.ForeignKey(Hortalizas, on_delete=models.CASCADE, related_name='asociadas_con')

    def __str__(self):
        return f"Asociación de {self.Id_hortaliza} a {self.Id_horta_aso}"

class Aso_ko(models.Model):
    Id_hortaliza = models.ForeignKey(Hortalizas, on_delete=models.CASCADE, related_name='no_asociadas')
    Id_horta_aso = models.ForeignKey(Hortalizas, on_delete=models.CASCADE, related_name='no_asociadas_con')

    def __str__(self):
        return f"Asociación de {self.Id_hortaliza} a {self.Id_horta_aso}"

class Cal_siembra(models.Model):
    Id_hortaliza = models.ForeignKey(Hortalizas, on_delete=models.CASCADE)
    Id_mes = models.ForeignKey(Mes, on_delete=models.CASCADE)

    def __str__(self):
        return f"Cálculo de Siembra para Hortaliza {self.Id_hortaliza} en el Mes {self.Id_mes}"

class Siembra(models.Model):
    Id = models.AutoField(primary_key=True)
    Id_celda = models.ForeignKey(Celda, on_delete=models.CASCADE)
    Id_huerto = models.ForeignKey(Huerto, on_delete=models.CASCADE)
    Id_hortaliza = models.ForeignKey(Hortalizas, on_delete=models.CASCADE)
    Fecha_siembra = models.DateTimeField()
    Fecha_cos_real = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"Siembra en Celda {self.Id_celda} en Huerto {self.Id_huerto} de {self.Id_hortaliza}"