from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = UserProfile
        fields = '__all__'

class HuertoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Huerto 
        fields = ('id','id_user', 'fecha_creacion', 'nombre')

class FamiliasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Familias
        fields = '__all__'

class TipoRiegoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tipo_riego
        fields = '__all__'

class MesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mes
        fields = '__all__'

class CeldaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Celda
        fields = '__all__'

class GastosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Gastos
        fields = '__all__'

class TareasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tareas
        fields = '__all__'

class PlagasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plagas
        fields = '__all__'

class RiegosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Riegos
        fields = '__all__'

class HortalizasSerializer(serializers.ModelSerializer):
    Tipo_riego = serializers.StringRelatedField()
    Familia = serializers.StringRelatedField()
    Id_horta_madre = serializers.StringRelatedField()

    class Meta:
        model = Hortalizas
        fields = '__all__'

class AsoOkSerializer(serializers.ModelSerializer):
    Id_hortaliza = serializers.StringRelatedField()
    Id_horta_aso = serializers.StringRelatedField()

    class Meta:
        model = Aso_ok
        fields = '__all__'

class AsoKoSerializer(serializers.ModelSerializer):
    Id_hortaliza = serializers.StringRelatedField()
    Id_horta_aso = serializers.StringRelatedField()

    class Meta:
        model = Aso_ko
        fields = '__all__'

class CalSiembraSerializer(serializers.ModelSerializer):
    Id_hortaliza = serializers.StringRelatedField()
    Id_mes = serializers.StringRelatedField()

    class Meta:
        model = Cal_siembra
        fields = '__all__'

class SiembraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Siembra
        fields = '__all__'

class EstadoTareaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado_tarea
        fields = '__all__'