import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { SnackbarProvider } from 'notistack';
import { createTheme, ThemeProvider } from '@material-ui/core';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Dashboard from './components/Dashboard';
import MisHuertos from './components/MisHuertos';
import HuertoDetalle from './components/HuertoDetalle';
import HortalizaDetalle from './components/HortalizaDetalle';
import Estadisticas from './components/Estadisticas';
import InventarioModal from './components/InventarioModal';
import GastosDetalle from './components/GastosDetalle';
import TareasDetalle from './components/TareasDetalle';
import Calendario from './components/Calendario';
import CalMeses from './components/CalMeses';
import CrearHortaliza from './components/CrearHortaliza';
import Perfil from './components/Perfil';
import MisHortalizas from './components/MisHortalizas';
import HortalizasSistema from './components/HortalizasSistema';
import HortalizasFamilia from './components/HortalizasFamilia';
import withAuthentication from './components/withAuthentication';

const AuthenticatedDashboard = withAuthentication(Dashboard);

const theme = createTheme({
  typography: {
    fontFamily: '"Calibri", Arial, sans-serif',
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <SnackbarProvider maxSnack={3}>
        <Router>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/dashboard" element={<AuthenticatedDashboard />} />
            <Route path="/mis_huertos" element={<MisHuertos />} />
            <Route path="/huerto/:id" element={<HuertoDetalle />} />
            <Route path="/hortalizas/:id" element={<HortalizaDetalle />} />
            <Route path="/estadisticas" element={<Estadisticas />} />
            <Route path="/huertos/:id/inventario" element={<InventarioModal />} />
            <Route path="/huertos/:id/gastos" element={<GastosDetalle />} />
            <Route path="/huertos/:id/tareas" element={<TareasDetalle />} />
            <Route path="/calendario" element={<Calendario />} />
            <Route path="/crear-hortaliza" element={<CrearHortaliza />} />
            <Route path="/perfil" element={<Perfil />} />
            <Route path="/cal_meses" element={<CalMeses />} />
            <Route path="/mis_hortalizas" element={<MisHortalizas />} />
            <Route path="/hortalizas_sistema" element={<HortalizasSistema />} />
            <Route path="/hortalizas_familia" element={<HortalizasFamilia />} />
          </Routes>
        </Router>
      </SnackbarProvider>
    </ThemeProvider>
  );
}

export default App;