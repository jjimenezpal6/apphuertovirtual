import React, { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useNavigate, Link } from 'react-router-dom';
import {
  Button,
  Card,
  CardActionArea,
  CardContent,
  Typography,
  makeStyles,
  Grid,
  Box,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import HortalizaImagen from './HortalizaImagen';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  listItem: {
    marginBottom: theme.spacing(1),
  },
  card: {
    minWidth: 230,
    borderRadius: 16,
    overflow: 'hidden',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1), 0 8px 16px rgba(0, 0, 0, 0.1)',
    transition: 'transform 0.2s',
    '&:hover': {
      transform: 'scale(1.05)',
    },
  },
  mediaContainer: {
    height: 150,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  media: {
    objectFit: 'contain',
    width: '100%',
    height: '100%',
  },
  sectionTitle: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: theme.spacing(2),
    fontSize: 26,
  },
  createButton: {
    alignSelf: 'center',
    marginTop: theme.spacing(2),
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: theme.spacing(4),
  },
}));

function MisHortalizas() {
  const classes = useStyles();
  const [misHortalizas, setMisHortalizas] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    fetch(`${baseURL}/api/hortalizas/mis_hortalizas/`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => setMisHortalizas(data))
      .catch(error => enqueueSnackbar(`Error al obtener mis hortalizas: ${error.message}`, { variant: 'error' }));
  }, []);

  const calculateGridWidth = () => {
    const baseWidth = 12;
    const cardsPerRow = Math.min(6, misHortalizas.length);
    const calculatedWidth = Math.floor(baseWidth / Math.max(cardsPerRow, 1)); // Asegura que al menos haya 1 tarjeta por fila
    return calculatedWidth;
  };
  

  return (
    <div className={classes.root} style={{ overflowX: 'hidden', overflowY: 'hidden' }}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content}>
          <Typography variant="h4" className={classes.sectionTitle} style={{ marginTop: 20 }}>
            Mis Hortalizas
          </Typography>
          <Button
            variant="contained"
            color="primary"
            className={classes.createButton}
            onClick={() => navigate('/crear-hortaliza')}
            style={{ marginBottom: 50 }}
          >
            CREAR HORTALIZA
          </Button>
          <Grid container spacing={2}>
            {misHortalizas.map((hortaliza) => (
              <Grid item xs={12} sm={calculateGridWidth()} md={calculateGridWidth()} lg={calculateGridWidth()} key={hortaliza.id}>
                <Card className={classes.card}>
                  <Link to={`/hortalizas/${hortaliza.Id}`} style={{ textDecoration: 'none' }}>
                    <CardActionArea>
                      <div className={classes.mediaContainer}>
                        <HortalizaImagen
                          nombre={hortaliza.Nombre}
                          className={classes.media}
                        />
                      </div>
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          {hortaliza.Nombre}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Link>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Box>
    </div>
  );
}

export default MisHortalizas;