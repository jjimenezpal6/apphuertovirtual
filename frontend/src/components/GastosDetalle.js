import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
  Card,
  Typography,
  makeStyles,
  Button,
  TextField,
  Grid,
  Snackbar,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Header from './Header';
import { SideMenu } from './SideMenu';
import { useParams, useNavigate } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
    marginLeft: 300,
    marginTop: theme.spacing(10),
  },
  addButton: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    gap: theme.spacing(2),
    alignItems: 'center',
  },
  totalGastos: {
    marginBottom: theme.spacing(2),
  },
  taskCard: {
    marginBottom: theme.spacing(2),
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: theme.spacing(1),
  },
  sideMenu: {
    width: 250,
    marginTop: theme.spacing(8),
    backgroundColor: theme.palette.background.default,
    position: 'fixed',
    height: '100%',
    overflowY: 'auto',
    zIndex: theme.zIndex.drawer,
  },
}));

const GastosDetalle = () => {
  const classes = useStyles();
  const { id } = useParams();
  const navigate = useNavigate();
  const [gastos, setGastos] = useState([]);
  const [newGastoNota, setNewGastoNota] = useState('');
  const [newGastoFecha, setNewGastoFecha] = useState('');
  const [newGastoPrecio, setNewGastoPrecio] = useState('');
  const [editingItemId, setEditingItemId] = useState(null);
  const [editingGastoNota, setEditingGastoNota] = useState('');  
  const [editingGastoFecha, setEditingGastoFecha] = useState('');
  const [editingGastoPrecio, setEditingGastoPrecio] = useState('');
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('');
  const [confirmDeleteDialogOpen, setConfirmDeleteDialogOpen] = useState(false);
  const [deletingItemId, setDeletingItemId] = useState(null);

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  const fetchData = async () => {
    try {
      const gastosResponse = await axios.get(`${baseURL}/api/huertos/${id}/gastos/`, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });
      const sortedGastos = gastosResponse.data
        .sort((a, b) => b.fecha.localeCompare(a.fecha));

      setGastos(sortedGastos);
    } catch (error) {
      console.error('Hubo un error al obtener los datos de gastos:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [id]);

  const handleAddGasto = () => {
    if (!newGastoNota || !newGastoFecha || !newGastoPrecio) {
      setSnackbarMessage('Por favor, complete todos los campos.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
      return;
    }

    axios
      .post(
        `${baseURL}/api/huertos/${id}/gastos/create/`,
        {
          id_huerto: id,
          nota: newGastoNota,
          fecha: newGastoFecha,
          precio: newGastoPrecio,
        },
        {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        }
      )
      .then((response) => {
        setGastos(prevGastos => {
          const updatedGastos = [...prevGastos, response.data];
          return updatedGastos.sort((a, b) => b.fecha.localeCompare(a.fecha));
        });

        setNewGastoNota('');
        setNewGastoFecha('');
        setNewGastoPrecio('');
        setSnackbarMessage('Gasto agregado con éxito.');
        setSnackbarSeverity('success');
        setSnackbarOpen(true);
      })
      .catch((error) => {
        console.error('Hubo un error al agregar un nuevo gasto:', error);
        setSnackbarMessage('Hubo un error al agregar un nuevo gasto.');
        setSnackbarSeverity('error');
        setSnackbarOpen(true);
      });
  };

  const handleDeleteGasto = (gastoId) => {
    setDeletingItemId(gastoId);
    setConfirmDeleteDialogOpen(true);
  };

  const handleConfirmDelete = async () => {
    setConfirmDeleteDialogOpen(false);
  
    try {
      await axios.delete(`${baseURL}/api/gastos/${deletingItemId}/delete/`, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });
  
      setDeletingItemId(null);
      setGastos((prevGastos) => prevGastos.filter((gasto) => gasto.id !== deletingItemId));
      setSnackbarMessage('Gasto eliminado correctamente.');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);
    } catch (error) {
      console.error('Hubo un error al eliminar el gasto:', error);
      setSnackbarMessage('Hubo un error al eliminar el gasto.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
    }
  };

  const handleCancelDelete = () => {
    setDeletingItemId(null);
    setConfirmDeleteDialogOpen(false);
  };

  const handleEditClick = (gasto) => {
    setEditingItemId(gasto.id);
    setEditingGastoNota(gasto.nota);
    setEditingGastoFecha(gasto.fecha);
    setEditingGastoPrecio(gasto.precio);
  };

  const handleSaveChanges = async () => {
    try {
      await axios.put(
        `${baseURL}/api/gastos/${editingItemId}/update/`,
        {
          nota: editingGastoNota,
          fecha: new Date(editingGastoFecha).toISOString(),
          precio: editingGastoPrecio,
        },
        {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        }
      );
      setEditingItemId(null);
      setSnackbarMessage('Cambios guardados con éxito.');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);
      fetchData();

    } catch (error) {
      console.error('Hubo un error al actualizar el gasto:', error);
      setSnackbarMessage('Hubo un error al actualizar el gasto.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
    }
  };

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  const totalGastos = gastos.reduce((total, gasto) => total + parseFloat(gasto.precio), 0);

  return (
    <div className={classes.root}>
      <Header />
      <Box className={classes.sideMenu}>
        <SideMenu />
      </Box>
      <div className={classes.content}>
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.addButton}>
            <TextField
              label="Nuevo Gasto"
              variant="outlined"
              fullWidth
              value={newGastoNota}
              onChange={(e) => setNewGastoNota(e.target.value)}
              style={{ width: '250px' }}
            />
            <TextField
              label="Fecha"
              type="date"
              variant="outlined"
              fullWidth
              value={newGastoFecha}
              onChange={(e) => setNewGastoFecha(e.target.value)}
              InputLabelProps={{ shrink: true }}
              style={{ width: '200px' }}
            />
            <TextField
              label="Precio"
              type="number"
              variant="outlined"
              fullWidth
              value={newGastoPrecio}
              onChange={(e) => setNewGastoPrecio(e.target.value)}
              style={{ width: '150px' }}
            />
            <Button
              variant="contained"
              color="primary"
              onClick={handleAddGasto}
            >
              Añadir Gasto
            </Button>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => navigate(`/huerto/${id}`)}
            >
              Volver al Huerto
            </Button>
          </Grid>
          <Grid item xs={12} className={classes.totalGastos}>
            <Typography variant="h6">Gasto Total: {totalGastos.toFixed(2)}</Typography>

          </Grid>
          {gastos.map((gasto) => (
            <Grid item xs={12} key={gasto.id}>
              <Card className={classes.taskCard}>
                {editingItemId === gasto.id ? (
                  <>
                    <TextField
                      label="Nota"
                      variant="outlined"
                      fullWidth
                      value={editingGastoNota}
                      onChange={(e) => setEditingGastoNota(e.target.value)}
                      style={{ width: '250px' }}
                    />
                    <TextField
                      label="Fecha"
                      type="date"
                      variant="outlined"
                      fullWidth
                      value={editingGastoFecha}
                      onChange={(e) => setEditingGastoFecha(e.target.value)}
                      InputLabelProps={{ shrink: true }}
                      style={{ width: '200px' }}
                    />
                    <TextField
                      label="Precio"
                      type="number"
                      variant="outlined"
                      fullWidth
                      value={editingGastoPrecio}
                      onChange={(e) => setEditingGastoPrecio(e.target.value)}
                      style={{ width: '150px' }}
                    />
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleSaveChanges}
                    >
                      Grabar
                    </Button>
                  </>
                ) : (
                  <>
                    <div>
                      <Typography variant="body1">{gasto.nota}</Typography>
                      <Typography variant="subtitle2">
                        Fecha: {new Date(gasto.fecha).toLocaleDateString()}
                      </Typography>
                      <Typography variant="subtitle2">
                        Precio: {gasto.precio}
                      </Typography>
                    </div>
                    <div>
                      <Button
                        variant="outlined"
                        color="secondary"
                        onClick={() => handleDeleteGasto(gasto.id)}
                      >
                        Eliminar Gasto
                      </Button>
                      <Button
                        variant="outlined"
                        color="primary"
                        onClick={() => handleEditClick(gasto)}
                      >
                        Editar
                      </Button>
                    </div>
                  </>
                )}
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <MuiAlert onClose={handleCloseSnackbar} severity={snackbarSeverity}>
          {snackbarMessage}
        </MuiAlert>
      </Snackbar>

      {confirmDeleteDialogOpen && (
        <Dialog open={confirmDeleteDialogOpen} onClose={handleCancelDelete}>
          <DialogTitle>Confirmar Eliminación</DialogTitle>
          <DialogContent>
            <DialogContentText>
              ¿Está seguro de que desea eliminar este gasto?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCancelDelete} color="primary">
              Cancelar
            </Button>
            <Button onClick={handleConfirmDelete} color="primary">
              Eliminar
            </Button>
          </DialogActions>
        </Dialog>
      )}

    </div>
  );
};

export default GastosDetalle;