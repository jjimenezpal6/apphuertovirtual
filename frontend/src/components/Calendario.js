import React, { useEffect, useState } from 'react';
import {
  Typography,
  makeStyles,
  Box,
  Paper,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  hortalizaItem: {
    marginBottom: theme.spacing(1),
    marginRight: theme.spacing(2),
    padding: theme.spacing(2),
    backgroundColor: '#EDE7D9',
    color: 'black',
    width: 'calc(25% - 8px)',
  },
  headerItem: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
    position: 'sticky',
    top: 0,
    zIndex: 1,
  },
  listItemBrown: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#D2B48C',
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  listItemGreen: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#C1E1C1',
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  hortalizaName: {
    fontSize: 20,
    flex: '1',
    color: 'black',
    fontWeight: 'bold',
  },
}));

function Calendario() {
  const [calendarioData, setCalendarioData] = useState([]);
  const [loading, setLoading] = useState(true);
  const classes = useStyles();

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/cal_siembra/`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        if (Array.isArray(data.data)) {
          const groupedData = groupBy(data.data, 'Id_hortaliza');
          setCalendarioData(groupedData);
        } else {
          throw new Error('La respuesta del servidor no contiene un array de datos.');
        }
        setLoading(false);
      })
      .catch(error => {
        console.error(`Error al obtener datos del calendario: ${error}`);
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <p>Cargando...</p>;
  }

  const allMonths = getMonthOptions();

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1}>
          <Typography variant="body1" style={{ fontSize: 26, marginTop: 20, marginLeft: 4 }}>
            Calendario de siembra por hortaliza
          </Typography>
          <Typography variant="body1" style={{ fontSize: 16, marginTop: 10, marginLeft: 4, marginBottom: 30 }}>
            Aquí se muestran las hortalizas y meses de siembra según el mes del año.
          </Typography>

          {/* Cabecera de la lista */}
          <Paper elevation={3} className={classes.headerItem}>
            <Typography variant="h6" style={{ fontSize: 20, flex: '1' }}>
              Hortaliza
            </Typography>
            {/* Cabecera de los meses */}
            {allMonths.map((month, index) => (
              <Typography key={index} variant="h6" style={{ fontSize: 20, flex: '1', textAlign: 'center' }}>
                {month.label}
              </Typography>
            ))}
          </Paper>

          {/* Elementos de la lista con colores alternados */}
          {Object.entries(calendarioData).map(([hortaliza, meses], index) => (
            <Paper
              elevation={3}
              key={index}
              className={index % 2 === 0 ? classes.listItemBrown : classes.listItemGreen}
            >
              <Typography variant="body1" className={classes.hortalizaName}>
                {hortaliza}
              </Typography>
              {/* Mostrar los meses en la cuadrícula */}
              {allMonths.map((month, monthIndex) => (
                <div key={monthIndex} style={{ textAlign: 'center', flex: '1' }}>
                  <FiberManualRecordIcon
                    style={{
                      fontSize: 20,
                      color: meses.includes(month.value) ? 'brown' : 'transparent',
                    }}
                  />
                </div>
              ))}
            </Paper>
          ))}
        </Box>
      </Box>
    </div>
  );
}

function groupBy(array, key) {
  return array.reduce((result, entry) => {
    const groupKey = entry[key];
    if (!result[groupKey]) {
      result[groupKey] = [];
    }
    result[groupKey].push(entry.Id_mes);
    return result;
  }, {});
}

function getMonthOptions() {
  return [
    { value: 'Enero', label: 'Enero' },
    { value: 'Febrero', label: 'Febrero' },
    { value: 'Marzo', label: 'Marzo' },
    { value: 'Abril', label: 'Abril' },
    { value: 'Mayo', label: 'Mayo' },
    { value: 'Junio', label: 'Junio' },
    { value: 'Julio', label: 'Julio' },
    { value: 'Agosto', label: 'Agosto' },
    { value: 'Septiembre', label: 'Septiembre' },
    { value: 'Octubre', label: 'Octubre' },
    { value: 'Noviembre', label: 'Noviembre' },
    { value: 'Diciembre', label: 'Diciembre' },
  ];
}

export default Calendario;