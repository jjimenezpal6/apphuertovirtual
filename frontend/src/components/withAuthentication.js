import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useSnackbar } from 'notistack';

function withAuthentication(Component) {
  return function AuthenticatedComponent(props) {
    const token = localStorage.getItem('token');
    const { enqueueSnackbar } = useSnackbar();
    const location = useLocation();

    if (!token) {
      enqueueSnackbar('No tienes permisos para acceder a esta página. Por favor, inicia sesión.', { variant: 'error' });
      return <Navigate to="/login" state={{ from: location }} />;
    }

    return <Component {...props} />;
  };
}

export default withAuthentication;