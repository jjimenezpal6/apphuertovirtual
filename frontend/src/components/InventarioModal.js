import React from 'react';
import {
  Modal,
  Backdrop,
  Fade,
  Paper,
  Typography,
  Button,
  makeStyles,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: '#fafafa',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(6),
    textAlign: 'center',
    borderRadius: theme.shape.borderRadius * 2,
    maxHeight: '80vh',
    overflowY: 'auto',
    maxWidth: '75%',
    width: '25%',
    [theme.breakpoints.up('md')]: {
      maxWidth: '80%',
    },
  },
  title: {
    color: theme.palette.text.primary,
    marginBottom: theme.spacing(4),
    fontWeight: 'bold',
    borderBottom: `2px solid ${theme.palette.secondary.main}`,
    paddingBottom: theme.spacing(2),
  },
  inventoryList: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    color: theme.palette.text.primary,
  },
  inventoryItem: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  boldText: {
    fontWeight: 'bold',
  },
  closeButton: {
    marginTop: theme.spacing(4),
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
}));

const InventarioModal = ({ open, handleClose, inventario }) => {
  const classes = useStyles();

  const sortedInventory = inventario
    ? inventario.sort((a, b) => b.total - a.total)
    : [];

  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Paper className={classes.paper}>
          <Typography variant="h4" className={classes.title}>
            Inventario del Huerto
          </Typography>
          {sortedInventory.length > 0 ? (
            <ul className={classes.inventoryList}>
              {sortedInventory.map((item, index) => (
                <li key={index} className={classes.inventoryItem}>
                  <span className={classes.boldText}>
                    {item.Id_hortaliza__Nombre}:
                  </span>{' '}
                  <span>{item.total}</span>
                </li>
              ))}
            </ul>
          ) : (
            <Typography variant="body1">
              No hay datos de inventario disponibles.
            </Typography>
          )}
          <Button
            onClick={handleClose}
            variant="contained"
            className={classes.closeButton}
          >
            Cerrar
          </Button>
        </Paper>
      </Fade>
    </Modal>
  );
};

export default InventarioModal;