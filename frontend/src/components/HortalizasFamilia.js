import React, { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';
import {
  Card,
  CardActionArea,
  CardContent,
  Typography,
  makeStyles,
  Grid,
  Box,
  CircularProgress,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import HortalizaImagen from './HortalizaImagen';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflowX: 'hidden',
    overflowY: 'hidden',
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  listItem: {
    marginBottom: theme.spacing(1),
  },
  card: {
    width: 200,
    borderRadius: 16,
    overflow: 'hidden',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
    transition: 'transform 0.2s',
    '&:hover': {
      transform: 'scale(1.05)',
    },
  },
  mediaContainer: {
    height: 150,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  media: {
    objectFit: 'contain',
    width: '100%',
    height: '100%',
    transition: 'transform 0.2s',
    '&:hover': {
      transform: 'scale(1.1)',
    },
  },
  sectionTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
    fontSize: 26,
  },
  loadingContainer: {
    position: 'fixed',
    top: '20%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    zIndex: 999,
  },
}));

function HortalizasFamilia() {
  const classes = useStyles();
  const [familias, setFamilias] = useState([]);
  const [hortalizasPorFamilia, setHortalizasPorFamilia] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { enqueueSnackbar } = useSnackbar();
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await fetch(`${baseURL}/api/familias/`, {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`
          }
        });

        if (!response.ok) {
          throw new Error(`Error al obtener las familias: ${response.statusText}`);
        }

        const data = await response.json();
        if (familias.length !== data.data.length) {
          setFamilias(data.data);
        }
      } catch (error) {
        enqueueSnackbar(`Error al obtener las familias: ${error.message}`, { variant: 'error' });
      } finally {
        setTimeout(() => {
          setIsLoading(false);
        }, 1500);
      }
    };

    fetchData();
  }, [enqueueSnackbar, familias]);

  useEffect(() => {
    if (Array.isArray(familias) && familias.length > 0) {
      const fetchHortalizasPorFamilias = async () => {
        const hortalizasData = [];
        for (const familia of familias) {
          try {
            const data = await getHortalizasPorFamilia(familia.id);
            hortalizasData.push({ familia, hortalizas: data });
          } catch (error) {
            console.error(`Error al obtener hortalizas para la familia ${familia.id}:`, error);
          }
        }
        setHortalizasPorFamilia(hortalizasData);
      };

      fetchHortalizasPorFamilias();
    }
  }, [familias]);

  const getHortalizasPorFamilia = async (familiaId) => {
    const url = `${baseURL}/api/hortalizas/familia/${familiaId}/`;

    try {
      const response = await fetch(url, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`
        }
      });

      if (!response.ok) {
        throw new Error(`Error al obtener hortalizas para la familia ${familiaId}`);
      }

      const data = await response.json();
      return data;
    } catch (error) {
      console.error(error);
      return [];
    }
  };

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content}>
          <Typography variant="h4" className={classes.sectionTitle}>
            Hortalizas por Familia
          </Typography>
          {isLoading && (
            <Box
              display="flex"
              alignItems="center"
              justifyContent="center"
              flexDirection="column"
              className={classes.loadingContainer}
            >
              <CircularProgress color="primary" size={50} />
              <Typography variant="h6" className={classes.loadingText}>
                Cargando Hortalizas...
              </Typography>
            </Box>
          )}
          {hortalizasPorFamilia.map(({ familia, hortalizas }) => (
            <div key={familia.id}>
              {hortalizas.length > 0 && (
              <>
                <Typography variant="h2"  className={classes.sectionTitle} style={{ marginTop: 30, marginBottom: 5, fontWeight: "bold" }} >
                  {familia.nombre}
                </Typography>
                <Divider />
              </>
              )}
              <Grid container spacing={2}>
                {hortalizas.map((hortaliza) => (
                  <Grid item key={hortaliza.id}>
                    <Card className={classes.card}>
                      <Link to={`/hortalizas/${hortaliza.Id}`} style={{ textDecoration: 'none' }}>
                        <CardActionArea>
                          <div className={classes.mediaContainer}>
                            <HortalizaImagen nombre={hortaliza.Nombre} />
                          </div>
                          <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                              {hortaliza.Nombre}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Link>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </div>
          ))}
        </Box>
      </Box>
    </div>
  );
}

export default HortalizasFamilia;