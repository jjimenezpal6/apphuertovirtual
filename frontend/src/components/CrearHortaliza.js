import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Typography,
  makeStyles,
  Box,
  Paper,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  Button,
  Snackbar,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Header from './Header';
import { SideMenu } from './SideMenu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 250,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paper: {
    padding: theme.spacing(3),
    width: '50%',
    minWidth: 300,
    maxWidth: 600,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  button: {
    marginTop: theme.spacing(2),
  },
}));

const CrearHortaliza = (props) => {
  const classes = useStyles();
  const [state, setState] = useState({
    Nombre: '',
    Tipo_riego: '',
    Familia: '',
    Id_user: localStorage.getItem('id_user'),
    Fecha_creacion: new Date().toISOString(),
    Dis_filas: '',
    Dis_plantas: '',
    Dias_a_cosecha: '',
    familias: [],
    tipos_riego: [],
    hortalizas: [],
  });

  const [validationErrors, setValidationErrors] = useState({});
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);

  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnackbar(false);
    setSuccessMessage('');
  };

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    axios
      .get(`${baseURL}/api/familias/`, {
        headers: { Authorization: `Token ${localStorage.getItem('token')}` },
      })
      .then((response) => {
        setState((prevState) => ({ ...prevState, familias: response.data.data }));
      })
      .catch((error) => console.log(error));

    axios
      .get(`${baseURL}/api/tipo_riego/`, {
        headers: { Authorization: `Token ${localStorage.getItem('token')}` },
      })
      .then((response) => {
        setState((prevState) => ({ ...prevState, tipos_riego: response.data.data }));
      })
      .catch((error) => console.log(error));

    axios
      .get(`${baseURL}/api/hortalizas/`, {
        headers: { Authorization: `Token ${localStorage.getItem('token')}` },
      })
      .then((response) => {
        setState((prevState) => ({ ...prevState, hortalizas: response.data.data }));
      })
      .catch((error) => console.log(error));
  }, []);

  const handleChange = (event) => {
    const { name, value } = event.target;

    setValidationErrors((prevErrors) => ({ ...prevErrors, [name]: undefined }));
    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const errors = {};
    if (!state.Nombre) errors.Nombre = 'Este campo es obligatorio';
    if (!state.Tipo_riego) errors.Tipo_riego = 'Este campo es obligatorio';
    if (!state.Familia) errors.Familia = 'Este campo es obligatorio';
    if (!state.Dis_filas) errors.Dis_filas = 'Este campo es obligatorio';
    if (!state.Dis_plantas) errors.Dis_plantas = 'Este campo es obligatorio';
    if (!state.Dias_a_cosecha) errors.Dias_a_cosecha = 'Este campo es obligatorio';
    if (Object.keys(errors).length > 0) {
      setValidationErrors(errors);
      setOpenSnackbar(true);
      setSuccessMessage(null);
      return;
    }

    try {
      const { Familia, Tipo_riego, Id_horta_madre, ...rest } = state;
      const requestData = {
        ...rest,
        Familia: Familia && Familia.id ? Familia.id : null,
        Tipo_riego_id: Tipo_riego && Tipo_riego.id ? Tipo_riego.id : null,
      };

      const response = await axios.post(`${baseURL}/api/hortalizas/`, requestData, {
        headers: { Authorization: `Token ${localStorage.getItem('token')}` },
      });

      if (props.history && props.history.push) {
        props.history.push('/hortalizas');
      }

      setState({
        Nombre: '',
        Tipo_riego: '',
        Familia: '',
        Id_user: localStorage.getItem('id_user'),
        Fecha_creacion: new Date().toISOString(),
        Dis_filas: '',
        Dis_plantas: '',
        Dias_a_cosecha: '',
        familias: [],
        tipos_riego: [],
        hortalizas: [],
      });

      setOpenSnackbar(true);
      setSuccessMessage('Hortaliza creada con éxito');
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1}>
          <Typography variant="h4" style={{marginTop: '50px'}} gutterBottom>
            Crear Hortaliza
          </Typography>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleSubmit}>
              <TextField
                label="Nombre"
                name="Nombre"
                onChange={handleChange}
                value={state.Nombre}
                error={Boolean(validationErrors.Nombre)}
                helperText={validationErrors.Nombre}
              />
              <InputLabel>Tipo de riego</InputLabel>
              <Select
                name="Tipo_riego"
                onChange={handleChange}
                value={state.Tipo_riego}
                error={Boolean(validationErrors.Tipo_riego)}
                helperText={validationErrors.Tipo_riego}
              >
                <MenuItem value="">--Selecciona un tipo de riego--</MenuItem>
                {state.tipos_riego.map((tipo) => (
                  <MenuItem key={tipo.id} value={tipo}>
                    {tipo.tipo}
                  </MenuItem>
                ))}
              </Select>
              <InputLabel>Familia</InputLabel>
              <Select
                name="Familia"
                onChange={handleChange}
                value={state.Familia}
                error={Boolean(validationErrors.Familia)}
                helperText={validationErrors.Familia}
              >
                <MenuItem value="">--Selecciona una familia--</MenuItem>
                {state.familias.map((familia) => (
                  <MenuItem key={familia.id} value={familia}>
                    {familia.nombre}
                  </MenuItem>
                ))}
              </Select>
              
              <TextField
                label="Distancia entre filas"
                type="number"
                name="Dis_filas"
                onChange={handleChange}
                value={state.Dis_filas}
                error={Boolean(validationErrors.Dis_filas)}
                helperText={validationErrors.Dis_filas}
              />
              <TextField
                label="Distancia entre plantas"
                type="number"
                name="Dis_plantas"
                onChange={handleChange}
                value={state.Dis_plantas}
                error={Boolean(validationErrors.Dis_plantas)}
                helperText={validationErrors.Dis_filas}
              />
              <TextField
                label="Días a la cosecha"
                type="number"
                name="Dias_a_cosecha"
                onChange={handleChange}
                value={state.Dias_a_cosecha}
                error={Boolean(validationErrors.Dias_a_cosecha)}
                helperText={validationErrors.Dias_a_cosecha}
              />
              <Button variant="contained" color="primary" className={classes.button} type="submit">
                Enviar
              </Button>
            </form>
          </Paper>
        </Box>
      </Box>
      <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleCloseSnackbar}
          severity={successMessage ? 'success' : 'error'}
        >
          {successMessage || 'Rellenar todos los campos'}
        </MuiAlert>
      </Snackbar>
    </div>
  );
};

export default CrearHortaliza;