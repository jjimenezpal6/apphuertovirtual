import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid } from '@material-ui/core';
import myImage from '../images/img_home.png';
import logoImage from '../images/logo_app.png';

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    backgroundColor: '#f4f4f4',
    minHeight: '100vh',
    overflow: 'hidden',
  },
  title: {
    margin: `${theme.spacing(8)}px 0 ${theme.spacing(4)}px`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#333',
    fontFamily: 'Lobster, sans-serif',
    fontSize: '3rem',
    fontWeight: 'bold',
    textShadow: '2px 2px 4px rgba(0, 0, 0, 0.2)',
  },
  logo: {
    marginRight: theme.spacing(2),
    width: '50px',
    height: '50px',
  },
  button: {
    margin: theme.spacing(2),
    fontSize: '1.2rem',
    padding: '15px 30px',
  },
  image: {
    maxWidth: '100%',
    height: 'auto',
    borderRadius: '8px',
    boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
    marginTop: '15px',
    marginBottom: '5px'
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '600px',
    margin: '0 auto',
  },
}));

function Home() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Lobster&family=Montserrat:wght@400&family=Pacifico&display=swap" />
      <Typography variant="h2" className={classes.title}>
        <img src={logoImage} alt="Logo" className={classes.logo} />
        HUERTO VIRTUAL
      </Typography>
      <Typography variant="h5" style={{whiteSpace: "pre"}}>
      La app que te permite crear y cuidar tu huerto desde tu dispositivo.  {<br />} ¡Disfruta de tu huerto y de sus frutos! 🥕🍅🥬
      </Typography>
      <Grid container spacing={3} className={classes.container}>
        <Grid item xs={12}>
          <img className={classes.image} src={myImage} alt="Huerto" />
        </Grid>
        <Grid item xs={12}>
          <Link to="/login">
            <Button className={classes.button} variant="contained" color="primary">
              Iniciar sesión
            </Button>
          </Link>
          <Link to="/register">
            <Button className={classes.button} variant="contained" color="primary">
              Registrarse
            </Button>
          </Link>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home;