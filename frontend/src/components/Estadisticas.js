import React, { useEffect, useState } from 'react';
import { Typography, makeStyles, Box, Paper } from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  listItem: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(1),
    backgroundColor: '#f8f9fa',
    borderRadius: theme.spacing(1),
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  statsHeader: {
    fontSize: 20,
    marginTop: 20,
    marginLeft: 4,
  },
  colorfulText: {
    color: '#FF4500',
  },
  podioContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginTop: 20,
    marginLeft: 4,
  },
  podioItem: {
    width: '30%',
    textAlign: 'center',
    border: '2px solid #ccc',
    padding: '10px',
    borderRadius: '8px',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)',
    marginBottom: 20,
  },
  oro: {
    background: '#FFD700',
    color: '#000000',
  },
  plata: {
    background: '#C0C0C0',
    color: '#000000',
  },
  bronce: {
    background: '#CD7F32',
    color: '#000000',
  },
}));

function Estadisticas() {
  const [estadisticas, setEstadisticas] = useState([]);
  const [loading, setLoading] = useState(true);
  const classes = useStyles();
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/estadisticas/`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setEstadisticas(data);
        setLoading(false);
      })
      .catch(error => {
        console.error(`Error al obtener estadísticas: ${error}`);
        setLoading(false);
      });
  }, []);

  if (loading) {
    return <p>Cargando...</p>;
  }

  const getTopThreeHortalizas = () => {
    if (estadisticas.length < 3) {
      console.warn('Faltan hortalizas para formar el podio');
      return [];
    }

    const sortedEstadisticas = [...estadisticas].sort((a, b) => b.total - a.total);
    return sortedEstadisticas.slice(0, 3);
  };

  const topThreeHortalizas = getTopThreeHortalizas();

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1}>
          <Typography variant="body1" style={{ fontSize: 26, marginTop: 20, marginLeft: 4 }}>
            Estadísticas
          </Typography>
          <Typography variant="body1" style={{ fontSize: 16, marginTop: 10, marginLeft: 4, marginBottom: 30 }}>
          Descubre el desglose detallado de todas las variedades de hortalizas que han sido sembradas en nuestros huertos virtuales.
          </Typography>

          <Typography variant="h6" className={classes.statsHeader}>
            Podio de las hortalizas más usadas:
          </Typography>
          {topThreeHortalizas.length === 0 ? (
            <Typography variant="body1" style={{ marginTop: 10, marginLeft: 4 }}>
              Faltan hortalizas para formar el podio.
            </Typography>
          ) : (
            <div className={classes.podioContainer}>
              <Paper className={`${classes.podioItem} ${classes.plata}`}>
                <Typography variant="h5">{topThreeHortalizas[1].Id_hortaliza__Nombre}</Typography>
                <Typography variant="h6">{topThreeHortalizas[1].total}</Typography>
              </Paper>
              <Paper className={`${classes.podioItem} ${classes.oro}`}>
                <Typography variant="h5">{topThreeHortalizas[0].Id_hortaliza__Nombre}</Typography>
                <Typography variant="h6">{topThreeHortalizas[0].total}</Typography>
              </Paper>
              <Paper className={`${classes.podioItem} ${classes.bronce}`}>
                <Typography variant="h5">{topThreeHortalizas[2].Id_hortaliza__Nombre}</Typography>
                <Typography variant="h6">{topThreeHortalizas[2].total}</Typography>
              </Paper>
            </div>
          )}

          <Typography variant="h6" className={classes.statsHeader}>
            Todas las Estadísticas:
          </Typography>
          <div style={{ marginBottom: '20px' }}>
            {estadisticas.map((estadistica, index) => (
              <Paper key={index} className={classes.listItem}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <Typography variant="body1" className={classes.colorfulText} style={{ marginRight: '8px' }}>
                    {estadistica.Id_hortaliza__Nombre}:
                  </Typography>
                  <Typography variant="body1">
                    {estadistica.total}
                  </Typography>
                </div>
              </Paper>    
            ))}
          </div>
        </Box>
      </Box>
    </div>
  );
}

export default Estadisticas;