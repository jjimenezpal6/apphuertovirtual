import React, { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import {
  Typography,
  makeStyles,
  Box,
  List,
  ListItem,
  ListItemText,
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  listItem: {
    marginBottom: theme.spacing(1),
    borderBottom: `1px solid ${theme.palette.divider}`,
    paddingBottom: theme.spacing(1),
  },
  createField: {
    marginTop: theme.spacing(2),
    borderBottom: `1px solid ${theme.palette.divider}`,
    paddingBottom: theme.spacing(1),
  },
  titulo: {
    marginTop: theme.spacing(4),
    paddingBottom: theme.spacing(1),
    fontSize: 10,
  },

}));

function MisHuertos() {
  const classes = useStyles();
  const [huertos, setHuertos] = useState([]);
  const [newHuertoName, setNewHuertoName] = useState('');
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const [deleteConfirmation, setDeleteConfirmation] = useState({ open: false, huertoId: null });

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  const handleCreateHuerto = () => {
    const id_user = localStorage.getItem('id_user');

    fetch(`${baseURL}/api/check_huerto_name/`, {
      method: 'POST',
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ nombre: newHuertoName }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Este nombre de huerto ya existe!');
        }

        return fetch(`${baseURL}/api/crear_huerto/`, {
          method: 'POST',
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ id_user, nombre: newHuertoName }),
        });
      })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Introduce un nombre para el nuevo huerto');
        }
        enqueueSnackbar('Huerto creado con éxito', { variant: 'success' });
        fetchHuertos();
        setNewHuertoName('');
      })
      .catch((error) => {
        enqueueSnackbar(error.message, { variant: 'error' });
      });
  };

  const handleDeleteHuerto = (huertoId) => {
    setDeleteConfirmation({ open: true, huertoId });
  };

  const confirmDeleteHuerto = () => {
    const { huertoId } = deleteConfirmation;
    fetch(`${baseURL}/api/huertos/${huertoId}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Error al eliminar el huerto');
        }
        enqueueSnackbar('Huerto eliminado con éxito', { variant: 'success' });
        fetchHuertos();
        setDeleteConfirmation({ open: false, huertoId: null });
      })
      .catch((error) => {
        enqueueSnackbar(error.message, { variant: 'error' });
        setDeleteConfirmation({ open: false, huertoId: null });
      });
  };

  const cancelDeleteHuerto = () => {
    setDeleteConfirmation({ open: false, huertoId: null });
  };

  const fetchHuertos = () => {
    fetch(`${baseURL}/api/mis_huertos/`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Error al obtener los huertos');
        }
        return response.json();
      })
      .then((data) => setHuertos(data))
      .catch((error) => {
        enqueueSnackbar(error.message, { variant: 'error' });
      });
  };

  useEffect(() => {
    fetchHuertos();
  }, [enqueueSnackbar]);

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1}>
          <Typography variant="body1" style={{ fontSize: 26, marginTop: 20, marginLeft: 20 }}>
            Mis huertos
          </Typography>
          <ListItem className={classes.createField}>
            <TextField
              fullWidth
              label="Crear nuevo huerto..."
              variant="outlined"
              value={newHuertoName}
              onChange={(e) => setNewHuertoName(e.target.value)}
            />
            <div style={{ marginLeft: '10px' }}>
              <Button variant="contained" style={{ backgroundColor: 'green', color: 'white' }} onClick={handleCreateHuerto}>
                CREAR
              </Button>
            </div>
          </ListItem>
          <List>
            {huertos.map((huerto, index) => (
              <React.Fragment key={huerto.id}>
                <ListItem className={classes.listItem}>
                  <ListItemText primary={huerto.nombre} secondary={`Fecha de creación: ${new Date(huerto.fecha_creacion).toLocaleDateString('es-ES')}`} />
                  <div style={{ marginRight: '20px' }}>
                    <Button variant="contained" color="primary" onClick={() => navigate(`/huerto/${huerto.id}`)}>
                      Ver
                    </Button>
                  </div>
                  <div style={{ marginLeft: '20px' }}>
                    <Button variant="contained" color="secondary" onClick={() => handleDeleteHuerto(huerto.id)}>
                      Eliminar
                    </Button>
                  </div>
                </ListItem>
              </React.Fragment>
            ))}
          </List>
          <Dialog open={deleteConfirmation.open} onClose={cancelDeleteHuerto}>
            <DialogTitle>Confirmar eliminación</DialogTitle>
            <DialogContent>
              <Typography>¿Está seguro que desea eliminar este huerto?</Typography>
            </DialogContent>
            <DialogActions>
              <Button onClick={cancelDeleteHuerto} color="primary">
                No
              </Button>
              <Button onClick={confirmDeleteHuerto} color="primary">
                Sí
              </Button>
            </DialogActions>
          </Dialog>
        </Box>
      </Box>
    </div>
  );
  
}

export default MisHuertos;