import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Typography, makeStyles, Box } from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import BlockIcon from '@material-ui/icons/Block';
import HortalizaImagen from './HortalizaImagen';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  sideMenu: {
    marginTop: theme.spacing(8),
    width: 250,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  detalleContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  tituloHortaliza: {
    marginBottom: theme.spacing(2),
  },
  detalleContent: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: theme.spacing(2),
  },
  imagenHortaliza: {
    width: 200,
    height: 200,
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  detalleInfo: {
    display: 'flex',
    flexDirection: 'column',
  },
  listItemBrown: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#D2B48C',
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  listItemGreen: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#C1E1C1',
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  listItemRed: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFB6C1',
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  hortalizaName: {
    fontSize: 20,
    flex: '1',
    color: 'black',
    fontWeight: 'bold',
  },
  listItemGray: {
    backgroundColor: 'gray',
  },
  listItemBlue: {
    marginBottom: theme.spacing(1),
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    borderRadius: 8,
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
  },
  asociacionesBox: {
    border: 1,
    borderColor: theme.palette.primary.main,
    borderRadius: 8,
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  asociacionesTitle: {
    color: theme.palette.primary.main,
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: theme.spacing(1),
    textAlign: 'center',
  },
  asociacionesList: {
    listStyleType: 'none',
    padding: 0,
    margin: 0,
    marginTop: theme.spacing(1),
    textAlign: 'center',
  },
  asociacionesListItem: {
    marginBottom: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
    color: 'black',
  },
  asociacionesIcon: {
    marginRight: theme.spacing(1),
    fontSize: 20,
    color: theme.palette.primary.main,
  },
  imagenHortaliza: {
    width: '300px',
    height: '300px',
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
}));

function HortalizaDetalle() {
  const { id } = useParams();
  const [hortaliza, setHortaliza] = useState(null);
  const [loadingHortaliza, setLoadingHortaliza] = useState(true);
  const [calendarioData, setCalendarioData] = useState([]);
  const [loadingCalendario, setLoadingCalendario] = useState(true);
  const [asoOkList, setAsoOkList] = useState([]);
  const [loadingAsoOk, setLoadingAsoOk] = useState(true);
  const classes = useStyles();
  const [asoKoList, setAsoKoList] = useState([]);
  const [loadingAsoKo, setLoadingAsoKo] = useState(true);
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/hortalizas_v2/${id}`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setHortaliza(data);
        setLoadingHortaliza(false);
      })
      .catch(error => {
        console.error(`Error fetching hortaliza details: ${error}`);
        setLoadingHortaliza(false);
      });
  }, [id]);

  useEffect(() => {
    if (!hortaliza) {
      return;
    }

    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/cal_siembra_hortaliza/${hortaliza.Id}`, { 
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setCalendarioData(data.data);
        setLoadingCalendario(false);
      })
      .catch(error => {
        console.error(`Error al obtener datos del calendario para la hortaliza: ${error}`);
        setLoadingCalendario(false);
      });
  }, [hortaliza]);

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }
 
    fetch(`${baseURL}/api/hortalizas_asociadas/${id}`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setAsoOkList(data.data);
        setLoadingAsoOk(false);
      })
      .catch(error => {
        console.error(`Error fetching aso_ok list: ${error}`);
        setLoadingAsoOk(false);
      });
  }, [id]);

  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/hortalizas_no_asociadas/${id}`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        setAsoKoList(data.data);
        setLoadingAsoKo(false);
      })
      .catch(error => {
        console.error(`Error fetching aso_ko list: ${error}`);
        setLoadingAsoKo(false);
      });
  }, [id]);

  if (loadingHortaliza || loadingCalendario || loadingAsoOk || loadingAsoKo) {
    return <p>Cargando...</p>;
  }

  if (!hortaliza) {
    return <p>No se pudo cargar la hortaliza.</p>;
  }
  
  const allMonths = getMonthOptions();

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content}>
          <div className={classes.detalleContainer}>
            <Box display="flex">
            <Box border={1} borderColor="grey" borderRadius={8} p={2} mt={10} mb={3} style={{ width: '500px', height: '500px', overflowY: 'auto' }}>
              <Typography variant="h6" gutterBottom style={{ marginTop: '-10px' }}>
                Ficha de la hortaliza
              </Typography>
              <Typography variant="h4" className={classes.tituloHortaliza}>
                {hortaliza.Nombre}
              </Typography>
              <div className={classes.detalleContent}>
                <div className={classes.imagenHortaliza}>
                  <HortalizaImagen
                    nombre={hortaliza.Nombre.toLowerCase()}
                  />
                </div>
                <div className={classes.detalleInfo}>
                  <p><strong>Hortaliza:</strong> {hortaliza.Nombre}</p>
                  <p><strong>Familia:</strong> {hortaliza.Familia}</p>
                  <p><strong>Tipo de Riego:</strong> {hortaliza.Tipo_riego}</p>
                  <p><strong>Distancia entre Filas [cm]:</strong> {hortaliza.Dis_filas}</p>
                  <p><strong>Distancia entre Plantas [cm]:</strong> {hortaliza.Dis_plantas}</p>
                  <p><strong>Días a Cosecha:</strong> {hortaliza.Dias_a_cosecha}</p>
                </div>
              </div>
            </Box>
            <Box display="flex" flexDirection="column" ml={2} mt={10}>
              <Box display="flex" flexDirection="row" mb={2}>
              <Box
                border={1}
                borderColor="blue"
                borderRadius={8}
                p={2}
                style={{ width: '250px', height: '500px', flex: '1', position: 'relative', overflowY: 'auto' }}
                className={classes.listItemGreen}
              >
                <div style={{ textAlign: 'center', position: 'absolute', top: '10px', left: '50%', transform: 'translateX(-50%)' }}>
                  <Typography variant="h6" className={classes.asociacionesTitle} style={{ whiteSpace: 'nowrap', borderBottom: '1px solid blue', marginBottom: '5px' }}>
                    <CheckCircleIcon className={classes.asociacionesIcon} />
                    Asociaciones Positivas
                  </Typography>
                </div>
                <div style={{ marginTop: '50px', textAlign: 'center' }}>
                  <ul className={classes.asociacionesList} style={{ maxHeight: '100%', overflowY: 'auto', marginTop: '8px' }}>
                    {asoOkList.map(asoOk => (
                      <li key={asoOk.Id} className={classes.asociacionesListItem} style={{ marginTop: '8px' }}>
                        <FiberManualRecordIcon className={classes.asociacionesIcon} />
                        {asoOk.Nombre}
                      </li>
                    ))}
                  </ul>
                </div>
              </Box>
              <Box
                border={1}
                borderColor="blue"
                borderRadius={8}
                p={2}
                ml={2}
                style={{ flex: '1', position: 'relative', width: '550px', overflowY: 'auto' }}
                className={classes.listItemRed}
              >
                <div style={{ textAlign: 'center', position: 'absolute', top: '10px', left: '50%', transform: 'translateX(-50%)' }}>
                  <Typography
                    variant="h6"
                    className={classes.asociacionesTitle}
                    style={{ whiteSpace: 'nowrap', borderBottom: '1px solid blue', marginBottom: '5px' }}
                  >
                    <BlockIcon className={classes.asociacionesIcon} />
                    Asociaciones Negativas
                  </Typography>
                </div>
                <div style={{ marginTop: '50px', textAlign: 'center' }}>
                  <ul className={classes.asociacionesList} style={{ maxHeight: '100%', overflowY: 'auto', marginTop: '8px' }}>
                    {asoKoList.map(asoKo => (
                      <li key={asoKo.Id} className={classes.asociacionesListItem} style={{ marginTop: '8px' }}>
                        <FiberManualRecordIcon className={classes.asociacionesIcon} />
                        {asoKo.Nombre}
                      </li>
                    ))}
                  </ul>
                </div>
              </Box>
              </Box>
            </Box>
            </Box>
            <div className={classes.detalleContent}>
              <Box border={1} borderColor="blue" borderRadius={8} p={2} ml={2} mt={2} style={{ flex: '1' }}>
                <Typography variant="h6" style={{ marginBottom: '14px' }}>Calendario de siembra</Typography>
                {loadingCalendario ? (
                  <p>Cargando el calendario de siembra...</p>
                ) : (
                  <div>
                    <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                      {allMonths.map((month, index) => (
                        <div
                          key={index}
                          className={`${classes.listItemBlue} ${calendarioData.includes(month.value) ? '' : classes.listItemGray}`}
                          style={{ marginRight: 8, marginBottom: 8, width: '60px' }}
                        >
                          <div style={{ textAlign: 'center', flex: '1' }}>
                            <Typography variant="caption">{month.label}</Typography>
                          </div>
                        </div>
                      ))}
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
                      {allMonths.map((month, monthIndex) => (
                        <div
                          key={monthIndex}
                          className={`${classes.listItemBrown} ${calendarioData.includes(month.value) ? '' : classes.listItemGray}`}
                          style={{ marginRight: 8, marginBottom: 8, width: '60px' }}
                        >
                          <div style={{ textAlign: 'center', flex: '1' }}>
                            <FiberManualRecordIcon
                              style={{
                                fontSize: 20,
                                color: calendarioData.includes(month.value) ? 'brown' : 'transparent',
                              }}
                            />
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </Box>
            </div>
          </div>
        </Box>
      </Box>
    </div>
  );
}

function getMonthOptions() {
  return [
    { value: 'Enero', label: 'Enero' },
    { value: 'Febrero', label: 'Febrero' },
    { value: 'Marzo', label: 'Marzo' },
    { value: 'Abril', label: 'Abril' },
    { value: 'Mayo', label: 'Mayo' },
    { value: 'Junio', label: 'Junio' },
    { value: 'Julio', label: 'Julio' },
    { value: 'Agosto', label: 'Agosto' },
    { value: 'Septiembre', label: 'Septiembre' },
    { value: 'Octubre', label: 'Octubre' },
    { value: 'Noviembre', label: 'Noviembre' },
    { value: 'Diciembre', label: 'Diciembre' },
  ];
}

export default HortalizaDetalle;