import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import LogoImage from '../images/logo_app.png';

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
    textAlign: 'center',
  },
  appBar: {
    top: '0',
  },
  logo: {
    marginRight: theme.spacing(2), 
    height: '50px',
  },
}));

export default function Header() {
  const navigate = useNavigate();
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const [username, setUsername] = useState('');
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('id_user');

    fetch(`${baseURL}/api/user/${userId}`, {
      method: 'GET',
      headers: {
        'Authorization': `Token ${token}`,
      },
    })
    .then(response => response.json())
    .then(data => setUsername(data.username));
  }, []);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    navigate('/');
  };

  const handleProfile = () => {
    navigate('/perfil');
  };

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <img src={LogoImage} alt="Logo" className={classes.logo} />
        <Typography variant="h5" className={classes.title}>
          HUERTO VIRTUAL
        </Typography>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="account of current user"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          style={{ cursor: 'default' }} 
        >
          <AccountCircle />
        </IconButton>
        <Typography variant="h6">
          {username}
        </Typography>
        <IconButton
          edge="end"
          color="inherit"
          aria-label="display more actions"
          onClick={handleMenu}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          keepMounted
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={handleClose}
        >
          <MenuItem onClick={handleProfile}>Perfil</MenuItem>
          <MenuItem onClick={handleLogout}>Cerrar sesión</MenuItem>
        </Menu>
      </Toolbar>
    </AppBar>
  );
}