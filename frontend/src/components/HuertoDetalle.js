import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import {
  Typography,
  makeStyles,
  Button,
  Grid,
  Box,
  TextField,
} from '@material-ui/core';

import Tomate from '../images/tomate.png';
import Pepino from '../images/pepino.png';
import Lechuga from '../images/lechuga.png';
import Ajo from '../images/ajo.png';
import Tomate1 from '../images/tomate1.png';
import Lechuga1 from '../images/lechuga1.jpg';
import Ajo1 from '../images/ajo1.jpg';
import Berenjena from '../images/berenjena.png';
import Calabaza from '../images/calabaza.png';
import Acelga from '../images/acelga.png';
import Fresa from '../images/fresa.png';
import Albahaca from '../images/albahaca.png';
import Alcachofa from '../images/alcachofa.png';
import Brócoli from '../images/brócoli.png';
import Calabacín from '../images/calabacín.png';
import Cebollino from '../images/cebollino.png';
import Coliflor from '../images/coliflor.png';
import Escarola from '../images/escarola.png';
import Melón from '../images/melón.png';
import Sandía from '../images/sandía.png';
import Maíz from '../images/maíz.png';
import Remolacha from '../images/remolacha.png';
import Patata from '../images/patata.png';
import Nabo from '../images/nabo.png';
import Canónigo from '../images/canónigo.png';
import Cebollas from '../images/cebollas.png';
import Col from '../images/col.png';
import Espinacas from '../images/espinacas.png';
import Apio from '../images/apio.png';
import Guisantes from '../images/guisantes.png';
import Habas from '../images/habas.png';
import Judías from '../images/judías.png';
import Perejil from '../images/perejil.png';
import Pimiento from '../images/pimiento.png';
import Rabanitos from '../images/rabanitos.png';
import Rúcula from '../images/rúcula.png';
import Zanahoria from '../images/zanahoria.png';
import Puerro from '../images/puerro.png';
import HortalizaUsuario from '../images/usuario.png';

import Header from './Header';
import { SideMenu } from './SideMenu';
import InventarioModal from './InventarioModal';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import StorageIcon from '@material-ui/icons/Storage';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import LocalFloristIcon from '@material-ui/icons/LocalFlorist';
import OpacityIcon from '@material-ui/icons/Opacity';
import BugReportIcon from '@material-ui/icons/BugReport';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import InfoModal from './InfoModal';
//import HortalizaImagen from './HortalizaImagen';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(8),
    overflowX: 'hidden',
    overflowY: 'hidden',
  },
  content: {
    flex: 1,
    overflow: 'hidden',
  },  
  sideMenu: {
    width: 240,
    marginRight: theme.spacing(4),
  },
  sideMenuContent: {
    padding: theme.spacing(2),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  grid: {
    width: '80%',
    height: '100%',
    display: 'grid',
    gridTemplateColumns: 'repeat(20, calc(100% / 20))',
    gridTemplateRows: 'repeat(20, calc(60vh / 20))',
    gap: theme.spacing(1),
  },
  box: {
    width: '100%',
    height: '100%',
    border: '1px solid black',
    boxSizing: 'border-box',
    position: 'relative',
  },
  boxRow: {
    display: 'flex',
    marginBottom: -2,
  },
  draggableBox: {
    width: 50,
    height: 50,
    border: '1px solid black',
    cursor: 'move',
    margin: 2,
    boxSizing: 'border-box',
  },
  selectedImage: {
    width: '100px',
    height: '100px',
  },
  image: {
    width: '80%',
    height: '80%',
    margin: '10%',
  },
  boxLabel: {
    position: 'absolute',
    color: 'black',
    fontWeight: 'bold',
    textShadow: '1px 1px white',
  },
  deleteButton: {
    backgroundColor: 'red',
    color: 'white',
  },
  deleteButtonOff: {
    backgroundColor: 'white',
    color: 'black',
  },
  mainContent: {
    paddingTop: theme.spacing(8),
  },
  selectedBoxContainer: {
    marginLeft: theme.spacing(4),
    width: '100%',
  },
   contextMenu: {
    zIndex: theme.zIndex.tooltip + 1,
  },
  cosecharButton: {
    backgroundColor: 'red',
    color: 'white',
  },
  contextMenu: {
    marginTop: '20px',
  },
  imageContainer: {
    position: 'relative',
    overflow: 'hidden',
  },
  imageWithBorder: {
    border: '3px solid #4CAF50',
    borderRadius: '8px',
  },
}));

const images = {
  Tomate,
  Pepino,
  Ajo,
  Lechuga,
  Tomate1,
  Ajo1,
  Lechuga1,
  Berenjena,
  Calabaza,
  Acelga,
  Fresa,
  Albahaca,
  Alcachofa,
  Brócoli,
  Calabacín,
  Cebollino,
  Coliflor,
  Escarola,
  Melón,
  Sandía,
  Maíz,
  Remolacha,
  Patata,
  Nabo,
  Apio,
  Canónigo,
  Cebollas,
  Col,
  Espinacas,
  Guisantes,
  Habas,
  Judías,
  Perejil,
  Pimiento,
  Rabanitos,
  Rúcula,
  Zanahoria,
  Puerro,
  HortalizaUsuario,
};

function HuertoDetalle() {
  const classes = useStyles();
  const [huerto, setHuerto] = useState(null);
  const { id } = useParams();
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedBox, setSelectedBox] = useState(null);
  const [selectedBoxName, setSelectedBoxName] = useState('');
  const [grid, setGrid] = useState(Array(400).fill(null));
  const [isDeleting, setIsDeleting] = useState(false);
  const [boxes, setBoxes] = useState([]);
  const [isInventarioModalOpen, setInventarioModalOpen] = useState(false);
  const [inventario, setInventario] = useState([]);
  const [isCosecharActive, setIsCosecharActive] = useState(false);
  const [isRiegoActive, setIsRiegoActive] = useState(false);
  const [isPlagaActive, setIsPlagaActive] = useState(false);
  const [isDeleteButtonDisabled, setIsDeleteButtonDisabled] = useState(false);
  const [isCosecharButtonDisabled, setIsCosecharButtonDisabled] = useState(false);
  const [selectedBoxType, setSelectedBoxType] = useState('');
  const [selectedBoxImage, setSelectedBoxImage] = useState('');

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  const getCellCoordinates = (index) => {
    const alphabetLength = 26;
    const column = String.fromCharCode(((index % 20) % alphabetLength) + 65);
    const row = Math.floor(index / 20) + 1;
    return `${column}${row}`;
  };

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setDrawerOpen(open);
  };

  const [searchTerm, setSearchTerm] = useState('');
  const [filteredBoxes, setFilteredBoxes] = useState(boxes);
  const [sistemaBoxes, setSistemaBoxes] = useState([]);
  const [searchTermSistema, setSearchTermSistema] = useState('');
  const [filteredSistemaBoxes, setFilteredSistemaBoxes] = useState(sistemaBoxes);
  const [isHortalizaSelected, setIsHortalizaSelected] = useState(false);
  const [infoModalOpen, setInfoModalOpen] = useState(false);
  const [selectedHortalizaInfo, setSelectedHortalizaInfo] = useState(null);
  const [isContextMenuVisible, setContextMenuVisibility] = useState(true);
  const [isVerInfoButtonVisible, setVerInfoButtonVisibility] = useState(true);
  const [contextMenuOpen, setContextMenuOpen] = useState(false);
  const [contextMenuPosition, setContextMenuPosition] = useState({ x: null, y: null });
  const [selectedCellIndex, setSelectedCellIndex] = useState(null);
  const [riegosData, setRiegosData] = React.useState(null);
  const [plagasData, setPlagasData] = React.useState(null);
  const [huertoIdModal, setHuertoIdModal] = useState(null);
  const [celdaIdModal, setCeldaIdModal] = useState(null);
  
  const handleContextMenu = (event, index) => {
    event.preventDefault();
    setVerInfoButtonVisibility(true);
    setContextMenuPosition({ x: event.clientX, y: event.clientY });
    setSelectedBox(grid[index] || null);
    setSelectedCellIndex(index);
    setContextMenuVisibility(!!grid[index]);
    setContextMenuOpen(true);
  };

  const handleCloseContextMenu = () => {
    setContextMenuOpen(false);
    setVerInfoButtonVisibility(false);
  };

  const handleVerInfoClick = async () => {
    setVerInfoButtonVisibility(false);
    try {
      if (selectedCellIndex === null || selectedCellIndex === undefined) {
        console.error('El índice de la celda no está definido.');
        return;
      }
  
      const index = selectedCellIndex;
      const huerto_id = id;
      const celda_id = `A${index + 1}`;
  
      if (grid[index]) {
        const apiUrlSiembra = `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/get_siembras`;
        const apiUrlRiegos = `${baseURL}/api/riegos/`;
        const apiUrlPlagas = `${baseURL}/api/plagas/`;
  
        const [siembraResponse, riegosResponse, plagasResponse] = await Promise.all([
          axios.get(apiUrlSiembra, {
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          }),
          axios.get(apiUrlRiegos, {
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          }),
          axios.get(apiUrlPlagas, {
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          }),
        ]);
  
        if (siembraResponse.data && riegosResponse.data && plagasResponse.data) {
          const siembraData = siembraResponse.data.siembra;
          const riegosData = riegosResponse.data;
          const plagasData = plagasResponse.data;
  
          setHuertoIdModal(huerto_id);
          setCeldaIdModal(celda_id);
          setInfoModalOpen(true);
          setSelectedHortalizaInfo({
            ...selectedBox,
            siembraData: {
              ...siembraData,
              Fecha_cos_real: siembraResponse.data.Fecha_cos_real,
            },
          });
  
          setRiegosData(riegosData);
          setPlagasData(plagasData);
          setSelectedBox(null);
        } else {
          console.error('Datos de respuesta de la API no válidos.');
        }
      } else {
        enqueueSnackbar(`La celda está vacía. No hay información de siembra.`, { variant: 'warning' });
      }
    } catch (error) {
      if (error.response) {
        console.error('Error de respuesta de la API:', error.response.data);
      } else if (error.request) {
        console.error('No se recibió respuesta de la API:', error.request);
      } else {
        console.error('Error al realizar la solicitud a la API:', error.message);
      }
    }
  };

  useEffect(() => {
    setFilteredBoxes(boxes.filter(box => box.name.toLowerCase().includes(searchTerm.toLowerCase())));
  }, [boxes, searchTerm]);

  useEffect(() => {
    setFilteredSistemaBoxes(sistemaBoxes.filter(box => box.name.toLowerCase().includes(searchTermSistema.toLowerCase())));
  }, [sistemaBoxes, searchTermSistema]);

  // Primer useEffect para obtener hortalizas del sistema
useEffect(() => {
  axios
    .get(`${baseURL}/api/hortalizas/sistema/`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    })
    .then((response) => {
      const newSistemaBoxes = response.data.map((hortaliza) => ({
        id: hortaliza.Id,
        name: hortaliza.Nombre,
        image: images[hortaliza.Nombre],
      }));
      setSistemaBoxes(newSistemaBoxes);
    })
    .catch((error) => {
      console.error('Hubo un error al obtener las hortalizas del sistema:', error);
    });
}, []);

// Segundo useEffect para obtener detalles del huerto
useEffect(() => {
  fetch(`${baseURL}/api/huertos/${id}`, {
    headers: {
      'Authorization': `Token ${localStorage.getItem('token')}`,
    },
  })
    .then((response) => response.json())
    .then((data) => {
      setHuerto(data);
    })
    .catch((error) => {
      enqueueSnackbar(error.message, { variant: 'error' });
    });
}, [id, enqueueSnackbar]);

// Tercer useEffect para obtener hortalizas del usuario
useEffect(() => {
  axios
    .get(`${baseURL}/api/hortalizas/mis_hortalizas/`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    })
    .then((response) => {
      const newBoxes = response.data.map((hortaliza) => ({
        id: hortaliza.Id,
        name: hortaliza.Nombre,
        image: images[HortalizaUsuario],
      }));
      setBoxes(newBoxes);
    })
    .catch((error) => {
      console.error('Hubo un error al obtener las hortalizas:', error);
    });
}, []);

// Cuarto useEffect para actualizar el grid
useEffect(() => {
  axios
    .get(`${baseURL}/api/huertos/${id}/siembras`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    })
    .then((response) => {
      const newGrid = Array(400).fill(null);
      response.data.forEach((siembra) => {
        const selectedBox = boxes.find((box) => box.id === siembra.Id_hortaliza) || sistemaBoxes.find((box) => box.id === siembra.Id_hortaliza);
        if (selectedBox) {
          selectedBox.Fecha_cos_real = siembra.Fecha_cos_real;
          const index = parseInt(siembra.Id_celda.substring(1)) - 1;
          newGrid[index] = selectedBox;
        }
      });
      setGrid(newGrid);
    })
    .catch((error) => {
      console.error('Hubo un error al obtener las siembras:', error);
    });
}, [id, boxes, sistemaBoxes]);

  useEffect(() => {
    // Llama al endpoint de inventario
    axios
      .get(`${baseURL}/api/huertos/${id}/inventario/`, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      })
      .then((response) => {
        setInventario(response.data.inventario);
      })
      .catch((error) => {
        console.error('Hubo un error al obtener el inventario:', error);
      });
  }, [id]);

  const handleBoxClick = (box) => {
    setIsDeleting(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setIsCosecharActive(false);
    setIsRiegoActive(false);
    setIsPlagaActive(false);
    setIsHortalizaSelected(true);
  
    let updatedType = 'Hortalizas del sistema';
  
    if (box.image === undefined) {
      updatedType = 'Hortalizas del usuario';
      const updatedBox = {
        ...box,
        image: HortalizaUsuario,
      };
  
      setSelectedBox(updatedBox);
      setSelectedBoxName(updatedBox.name);
      setSelectedBoxImage(updatedBox.image);
      setSelectedBoxType(updatedType);
    } else {
      setSelectedBox(box);
      setSelectedBoxName(box.name);
      setSelectedBoxImage(box.image);
      setSelectedBoxType(updatedType);
    }
  };

  const handleGridClick = (event, index) => {
    event.preventDefault();
  
    if (isDeleting) {
      // Lógica para Borrar
      const newGrid = [...grid];
      newGrid[index] = null;
  
      if (grid[index]) {
        axios
          .delete(`${baseURL}/api/huertos/${id}/celdas/A${index + 1}/siembras/delete`, {
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          })
          .then(() => {
            setGrid(newGrid);
            console.log(`Siembra eliminada correctamente en la celda A${index + 1}`);
            enqueueSnackbar(`Siembra eliminada correctamente en la celda A${index + 1}`, { variant: 'success' });
          })
          .catch((error) => {
            console.error(`Hubo un error al eliminar la siembra de la celda A${index + 1}:`, error);
          });
      } else {
        setGrid(newGrid);
        console.log(`No hay siembra para eliminar en la celda A${index + 1}`);
        enqueueSnackbar(`La celda está vacía. No se puede eliminar.`, { variant: 'warning' });
      }
    } else if (isCosecharActive) {
      // Lógica para Cosechar
      if (grid[index]) {
        const hortalizaId = grid[index].id;
        axios
          .put(
            `${baseURL}/api/huertos/${id}/celdas/A${index + 1}/siembras/manage`,
            {
              Id_hortaliza: hortalizaId,
              Fecha_cos_real: new Date().toISOString(),
            },
            {
              headers: {
                'Authorization': `Token ${localStorage.getItem('token')}`,
              },
            }
          )
          .then(() => {
            enqueueSnackbar(`Cosecha realizada correctamente en la celda A${index + 1}`, { variant: 'success' });
          })
          .catch((error) => {
            console.error(`Hubo un error al realizar la cosecha en la celda A${index + 1}:`, error);
          });
      } else {
        console.log(`No hay siembra para cosechar en la celda A${index + 1}`);
        enqueueSnackbar(`La celda está vacía. No se puede cosechar.`, { variant: 'warning' });
      }
    } else if (isPlagaActive) {
      // Lógica para Plagas
      if (grid[index]) {
        const huerto_id = id;
        const celda_id = `A${index + 1}`;
  
        axios
          .post(
            `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/plagas/create`,
            {},
            {
              headers: {
                'Authorization': `Token ${localStorage.getItem('token')}`,
              },
            }
          )
          .then(() => {
            enqueueSnackbar(`Plaga añadida correctamente en la celda ${celda_id}`, { variant: 'success' });
          })
          .catch((error) => {
            console.error(`Hubo un error al registrar la plaga en la celda ${celda_id}:`, error);
          });
      } else {
        enqueueSnackbar(`La celda está vacía. No se puede añadir una plaga.`, { variant: 'warning' });
      }
    } else if (isRiegoActive) {
      // Lógica para Riegos
      if (grid[index]) {
        const huerto_id = id;
        const celda_id = `A${index + 1}`;
  
        axios
          .post(
            `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/riegos/create`,
            {},
            {
              headers: {
                'Authorization': `Token ${localStorage.getItem('token')}`,
              },
            }
          )
          .then(() => {
            enqueueSnackbar(`Riego añadido correctamente en la celda ${celda_id}`, { variant: 'success' });
          })
          .catch((error) => {
            console.error(`Hubo un error al registrar el riego en la celda ${celda_id}:`, error);
          });
      } else {
        enqueueSnackbar(`La celda está vacía. No se puede añadir un riego.`, { variant: 'warning' });
      }
    } else if (selectedBox) {
      // Lógica para Agregar siembra
      const newGrid = [...grid];
      newGrid[index] = selectedBox;
      setGrid(newGrid);
    } else {
      console.log('Ninguna lógica ejecutada');
    }
  };

  const handleClearClick = () => {
    setIsDeleting(!isDeleting);
    setIsCosecharActive(false);
    setIsRiegoActive(false);
    setIsPlagaActive(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setIsHortalizaSelected(false);
  };

  const handleCosecharClick = () => {
    setIsCosecharActive(!isCosecharActive);
    setIsRiegoActive(false);
    setIsPlagaActive(false);
    setIsDeleting(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setIsHortalizaSelected(false);
  };

  const handleRiegoClick = () => {
    setIsRiegoActive(!isRiegoActive);
    setIsPlagaActive(false);
    setIsDeleting(false);
    setIsCosecharActive(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setIsHortalizaSelected(false);
  };

  const handlePlagaClick = () => {
    setIsPlagaActive(!isPlagaActive);
    setIsRiegoActive(false);
    setIsDeleting(false);
    setIsCosecharActive(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setIsHortalizaSelected(false);
  };

  const handleGrabarClick = () => {
    setIsCosecharActive(false);
    setIsRiegoActive(false);
    setIsPlagaActive(false);
    setIsDeleting(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
  
    grid.forEach((cell, index) => {
      if (cell) {
        const huerto_id = id;
        const celda_id = `A${index + 1}`;
        const requestData = {
          Id_hortaliza: cell.id,
        };
  
        axios
          .post(
            `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/siembras`,
            requestData,
            {
              headers: {
                'Authorization': `Token ${localStorage.getItem('token')}`,
              },
            }
          )
          .then((response) => {
            console.log(`Siembra realizada en la celda ${celda_id}:`, response.data);
          })
          .catch((error) => {
            const updateData = {
              Id_hortaliza: cell.id,
            };
  
            if (cell.id !== requestData.Id_hortaliza) {
              updateData.Fecha_cos_real = new Date().toISOString();
            }
  
            axios
              .put(
                `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/siembras/update`,
                updateData,
                {
                  headers: {
                    'Authorization': `Token ${localStorage.getItem('token')}`,
                  },
                }
              )
              .then((response) => {
                console.log(`Siembra actualizada en la celda ${celda_id}:`, response.data);
              })
              .catch((error) => {
                console.error(`Hubo un error al actualizar la siembra en la celda ${celda_id}:`, error);
              });
          });
      }
    });
  
    setIsDeleting(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    enqueueSnackbar('Cambios grabados correctamente', { variant: 'success' });
  };

  const handleCosecharFromContextMenu = (event, index) => {
    event.preventDefault();
    handleCloseContextMenu();
  
    if (selectedBox ) {
      const huerto_id = id;
      const rowNumber = Math.floor(index / 20) + 1;
      const colNumber = (index % 20) + 1;
      const celda_id = `A${rowNumber}${colNumber}`;
      axios
        .put(
          `${baseURL}/api/huertos/${huerto_id}/celdas/${celda_id}/siembras/manage`,
          {
            Id_hortaliza: selectedBox.id,
            Fecha_cos_real: new Date().toISOString(),
          },
          {
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          }
        )
        .then(() => {
          const newGrid = [...grid];
          newGrid[index] = selectedBox;
          setGrid(newGrid);
          console.log(`Cosecha realizada correctamente en la celda ${celda_id}`);
        })
        .catch((error) => {
          console.error(`Hubo un error al realizar la cosecha en la celda ${celda_id}:`, error);
        });
    }
  };

  if (!huerto) {
    return null;
  }

  const handleInventarioClick = () => {
    setIsCosecharActive(false);
    setIsRiegoActive(false);
    setIsPlagaActive(false);
    setIsDeleting(false);
    setIsDeleteButtonDisabled(false);
    setIsCosecharButtonDisabled(false);
    setInventarioModalOpen(true);
  };

  const handleCloseInfoModal = () => {
    setInfoModalOpen(false);
    setContextMenuVisibility(true);
  };

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex" alignItems="flex-start">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1} style={{ marginTop: '4px' }}>
          <Grid container spacing={2}>
            {/* Bloque de títulos y botón Grabar */}
            <Grid item xs={6}>
              <Typography variant="h5" component="h2">
               HUERTO: {huerto.nombre}
              </Typography>
              <Typography color="textSecondary">
                Fecha de creación: {new Date(huerto.fecha_creacion).toLocaleDateString('es-ES')}
              </Typography>
              <Button
                onClick={handleGrabarClick}
                className={classes.menuButton}
                variant="contained"
                color="primary"
                startIcon={<SaveIcon />}
                style={{ marginTop: '10px' , marginBottom: '-20px' }}
              >
                Grabar
              </Button>
            </Grid>
            {/* Bloque de botones de Ver Inventario, Tareas y Gastos */}
            <Grid item xs={6} container spacing={1} alignItems="flex-end" justifyContent="flex-end" style={{ marginBottom: '25px' }}>
              <Grid item>
                <Button
                  onClick={handleInventarioClick}
                  className={classes.menuButton}
                  variant="contained"
                  color="default"
                  startIcon={<StorageIcon />}
                >
                  Ver Inventario
                </Button>
              </Grid>
              <Grid item>
                <Button
                  component={Link}
                  to={`/huertos/${id}/tareas`}
                  className={classes.menuButton}
                  variant="contained"
                  color="default"
                  startIcon={<PlaylistAddCheckIcon />}
                >
                  TAREAS
                </Button>
              </Grid>
              <Grid item>
                <Button
                  component={Link}
                  to={`/huertos/${id}/gastos`}
                  className={classes.menuButton}
                  variant="contained"
                  color="default"
                  startIcon={<MonetizationOnIcon />}
                >
                  GASTOS
                </Button>
              </Grid>
            </Grid>
          </Grid>
          {/* Bloque de botones de Borrar, Cosechar, Regar y Plagas */}
          <Grid container spacing={1} alignItems="flex-end" justifyContent="flex-end" style={{ marginBottom: '-20px' }}>
            <Typography variant="h5" component="h2">
               SELECCIONA UN MODO:
            </Typography>
            <Grid item>
              <Button
                onClick={handleClearClick}
                className={`${classes.menuButton} ${isDeleting ? classes.deleteButton : classes.deleteButtonOff}`}
                variant="contained"
                color="default"
                startIcon={<DeleteIcon />}
                style={{ marginLeft: '20px' }}
              >
                Borrar
              </Button>
            </Grid>
            <Grid item>
              <Button
                onClick={handleCosecharClick}
                className={`${classes.menuButton} ${isCosecharActive ? classes.cosecharButton : classes.deleteButtonOff}`}
                variant="contained"
                color="default"
                startIcon={<LocalFloristIcon />}
              >
                Cosechar
              </Button>
            </Grid>
            <Grid item>
              <Button
                onClick={handleRiegoClick}
                className={`${classes.menuButton} ${isRiegoActive ? classes.cosecharButton : classes.deleteButtonOff}`}
                variant="contained"
                color="default"
                startIcon={<OpacityIcon />}
              >
                RIEGOS
              </Button>
            </Grid>
            <Grid item>
              <Button
                onClick={handlePlagaClick}
                className={`${classes.menuButton} ${isPlagaActive ? classes.cosecharButton : classes.deleteButtonOff}`}
                variant="contained"
                color="default"
                startIcon={<BugReportIcon />}
              >
                PLAGAS
              </Button>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Grid container spacing={0} className={classes.grid} style={{ marginTop: '5px', marginBottom: '10px' }}>
                {grid.map((cell, index) => {
                  return (
                    <Grid
                      item
                      key={index}
                      className={classes.box}
                      onClick={(event) => handleGridClick(event, index)}
                      onContextMenu={(event) => handleContextMenu(event, index)}
                    >
                      {cell && (
                        <img
                          src={cell.image === undefined ? HortalizaUsuario : cell.image}
                          alt=""
                          title={cell.name}
                          className={classes.image}
                        />
                      )}
                    </Grid>
                  );
                })}
                {Array(20 * 20 - grid.length).fill(null).map((_, index) => (
                  <Grid
                    item
                    key={index + grid.length}
                    className={classes.box}
                    onClick={() => handleGridClick(index + grid.length)}
                    onContextMenu={(event) => handleContextMenu(event, index + grid.length)}
                  >
                  </Grid>
                ))}
              </Grid>
            </Grid>
            <Grid item xs={6}>
            <Grid item>
              {isHortalizaSelected && (
                <>
                  <Typography variant="h6" style={{ marginTop: '30px' }} >Hortaliza seleccionada: {selectedBoxName}</Typography>
                  {selectedBoxType === 'Hortalizas del usuario' ? (
                    <div className={classes.imageContainer}>
                      <img
                        src={HortalizaUsuario}
                        alt={selectedBoxName}
                        className={`${classes.selectedImage} ${classes.imageWithBorder}`}
                      />
                    </div>
                  ) : (
                    selectedBoxImage && (
                      <div className={classes.imageContainer}>
                        <img
                          src={selectedBoxImage}
                          alt={selectedBoxName}
                          className={`${classes.selectedImage} ${classes.imageWithBorder}`}
                        />
                      </div>
                    )
                  )}
                </>
              )}
            </Grid>
              <Grid item>
                <Grid item>
                  <Typography variant="h6" style={{ marginTop: '40px' }}>Hortalizas del sistema</Typography>
                </Grid>
                <Grid item>
                  <TextField
                    label="Buscar Hortaliza"
                    variant="outlined"
                    fullWidth
                    value={searchTermSistema}
                    onChange={(e) => setSearchTermSistema(e.target.value)}
                    style={{ marginBottom: '10px' }}
                  />
                  <Grid container spacing={1}>
                    {filteredSistemaBoxes.map((box, index) => (
                      <Grid item xs={1} key={index}>
                        <div
                          className={`${classes.draggableBox} ${selectedBox === box ? classes.selectedBox : ''}`}
                          onClick={() => handleBoxClick(box)}
                        >
                          <img
                            src={box.image}
                            alt=""
                            title={box.name}
                            className={`${classes.image} ${selectedBox === box ? classes.selectedImage : ''}`}
                          />
                        </div>
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Typography variant="h6" style={{ marginTop: '40px' }}>Mis Hortalizas</Typography>
                <TextField
                  label="Buscar Hortaliza"
                  variant="outlined"
                  fullWidth
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                  style={{ marginBottom: '10px' }}
                />
                <Grid container spacing={1}>
                  {filteredBoxes.map((box, index) => (
                    <Grid item xs={1} key={index}>
                      <div
                        className={`${classes.draggableBox} ${selectedBox === box ? classes.selectedBox : ''}`}
                        onClick={() => handleBoxClick(box)}
                      >
                        <img
                          src={HortalizaUsuario}
                          alt=""
                          title={box.name}
                          className={`${classes.image} ${selectedBox === box ? classes.selectedImage : ''}`}
                        />
                      </div>
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Typography variant="caption" style={{ marginTop: '10px', fontSize: '12px' }}> 
            * Pulse click derecho sobre una celda con hortaliza para ver más información
          </Typography>
          {isVerInfoButtonVisible && isContextMenuVisible && (
            <Menu
              className={classes.contextMenu}
              keepMounted
              open={contextMenuOpen}
              onClose={handleCloseContextMenu}
              anchorReference="anchorPosition"
              anchorPosition={
                contextMenuPosition.y !== null && contextMenuPosition.x !== null
                  ? { top: contextMenuPosition.y, left: contextMenuPosition.x }
                  : undefined
              }
            >
              <MenuItem onClick={(event) => handleVerInfoClick()}>Ver Info</MenuItem>
            </Menu>
          )}
          <InventarioModal
            open={isInventarioModalOpen}
            handleClose={() => setInventarioModalOpen(false)}
            inventario={inventario}
          />
        </Box>
      </Box>
      <InfoModal
        open={infoModalOpen}
        handleClose={handleCloseInfoModal}
        hortalizaInfo={selectedHortalizaInfo}
        siembraData={selectedHortalizaInfo ? selectedHortalizaInfo.siembraData : null}
        huertoIdModal={huertoIdModal}
        celdaIdModal={celdaIdModal}
      />
    </div>
  );
}

export default HuertoDetalle;