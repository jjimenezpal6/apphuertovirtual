import React, { useEffect, useState } from 'react';
import {
  Typography,
  makeStyles,
  Box,
  Paper,
  Button,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import HortalizaImagen from './HortalizaImagen';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  hortalizaItem: {
    marginBottom: theme.spacing(2),
    marginRight: theme.spacing(2),
    padding: theme.spacing(2),
    backgroundColor: '#EDE7D9',
    color: 'black',
    width: 'calc(16.66% - 8px)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height: '240px',
  },
  hortalizaImage: {
    width: '75%',
    height: '75%',
    objectFit: 'contain',
    marginBottom: theme.spacing(1),
  },
  hortalizaText: {
    fontSize: 22,
    marginBottom: theme.spacing(1),
    fontWeight: 'bold',
  },
}));

function CalMeses() {
  const [loading, setLoading] = useState(true);
  const [selectedMonth, setSelectedMonth] = useState('');
  const [calendarioData, setCalendarioData] = useState([]);
  const [hortalizasPorMes, setHortalizasPorMes] = useState([]);
  const classes = useStyles();

  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  
  useEffect(() => {
    const token = localStorage.getItem('token');

    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/cal_siembra/`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        if (Array.isArray(data.data)) {
          const groupedData = groupBy(data.data, 'Id_hortaliza');
          setCalendarioData(groupedData);
        } else {
          throw new Error('La respuesta del servidor no contiene un array de datos.');
        }
        setLoading(false);
      })
      .catch(error => {
        console.error(`Error al obtener datos del calendario: ${error}`);
        setLoading(false);
      });
  }, []);

  const handleMonthButtonClick = (month) => {
    setSelectedMonth(month);
    fetchHortalizasPorMes(month);
  };

  const fetchHortalizasPorMes = (selectedMonth) => {
    const token = localStorage.getItem('token');
    if (!token) {
      console.error('Token de autenticación no encontrado');
      return;
    }

    fetch(`${baseURL}/api/cal_siembra/?Id_mes=${selectedMonth}`, {
      headers: {
        'Authorization': `Token ${token}`
      }
    })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then(data => {
        if (Array.isArray(data.data)) {
          const filteredData = data.data.filter(item => item.Id_mes === selectedMonth);
          const groupedData = groupBy(filteredData, 'Id_hortaliza');
          setHortalizasPorMes(groupedData);
        } else {
          throw new Error('La respuesta del servidor no contiene un array de datos.');
        }
      })
      .catch(error => {
        console.error(`Error al obtener datos del calendario por mes: ${error}`);
      });
  };

  if (loading) {
    return <p>Cargando...</p>;
  }

  const allMonths = getMonthOptions();

  return (
    <div className={classes.root}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content} flexGrow={1}>
          <Typography variant="body1" style={{ fontSize: 26, marginTop: 20, marginLeft: 4 }}>
            Calendario de siembra por meses
          </Typography>
          <Typography variant="body1" style={{ fontSize: 16, marginTop: 10, marginLeft: 4, marginBottom: 30 }}>
            Aquí se muestran las hortalizas que puedes sembrar según el mes elegido.
          </Typography>
          <div style={{ display: 'flex', flexWrap: 'wrap', marginBottom: 10 }}>
            {allMonths.map((month) => (
              <Button
                key={month.value}
                variant={selectedMonth === month.value ? 'contained' : 'outlined'}
                color="primary"
                onClick={() => handleMonthButtonClick(month.value)}
                style={{ marginRight: 20 , marginBottom: 20}}
              >
                {month.label}
              </Button>
            ))}
          </div>
          {selectedMonth ? (
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
              {Object.entries(hortalizasPorMes).map(([hortaliza, meses], index) => (
                <Paper elevation={3} key={index} className={classes.hortalizaItem}>
                  <Typography variant="body1" className={classes.hortalizaText}>
                    {hortaliza}
                  </Typography>
                  <HortalizaImagen nombre={hortaliza} className={classes.hortalizaImage} />
                </Paper>
              ))}
            </div>
          ) : (
            <Typography variant="body1" className={classes.message}>
              Selecciona un mes para ver las hortalizas que puedes sembrar.
            </Typography>
          )}
        </Box>
      </Box>
    </div>
  );
}

const groupBy = (array, key) => {
  return array.reduce((result, entry) => {
    const groupKey = entry[key];
    if (!result[groupKey]) {
      result[groupKey] = [];
    }
    result[groupKey].push(entry.Id_mes);
    return result;
  }, {});
};

const getMonthOptions = () => {
  return [
    { value: 'Enero', label: 'Enero' },
    { value: 'Febrero', label: 'Febrero' },
    { value: 'Marzo', label: 'Marzo' },
    { value: 'Abril', label: 'Abril' },
    { value: 'Mayo', label: 'Mayo' },
    { value: 'Junio', label: 'Junio' },
    { value: 'Julio', label: 'Julio' },
    { value: 'Agosto', label: 'Agosto' },
    { value: 'Septiembre', label: 'Septiembre' },
    { value: 'Octubre', label: 'Octubre' },
    { value: 'Noviembre', label: 'Noviembre' },
    { value: 'Diciembre', label: 'Diciembre' },
  ];
};

export default CalMeses;