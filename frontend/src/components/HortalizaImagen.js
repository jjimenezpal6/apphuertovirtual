import React from 'react';
import Usuario from '../images/usuario.png';
import Tomate from '../images/tomate.png';
import Pepino from '../images/pepino.png';
import Lechuga from '../images/lechuga.png';
import Ajo from '../images/ajo.png';
import Tomate1 from '../images/tomate1.png';
import Lechuga1 from '../images/lechuga1.jpg';
import Ajo1 from '../images/ajo1.jpg';
import Berenjena from '../images/berenjena.png';
import Calabaza from '../images/calabaza.png';
import Acelga from '../images/acelga.png';
import Fresa from '../images/fresa.png';
import Albahaca from '../images/albahaca.png';
import Alcachofa from '../images/alcachofa.png';
import Brócoli from '../images/brócoli.png';
import Calabacín from '../images/calabacín.png';
import Cebollino from '../images/cebollino.png';
import Coliflor from '../images/coliflor.png';
import Escarola from '../images/escarola.png';
import Melón from '../images/melón.png';
import Sandía from '../images/sandía.png';
import Maíz from '../images/maíz.png';
import Remolacha from '../images/remolacha.png';
import Patata from '../images/patata.png';
import Nabo from '../images/nabo.png';
import Canónigo from '../images/canónigo.png';
import Cebollas from '../images/cebollas.png';
import Col from '../images/col.png';
import Espinacas from '../images/espinacas.png';
import Apio from '../images/apio.png';
import Guisantes from '../images/guisantes.png';
import Habas from '../images/habas.png';
import Judías from '../images/judías.png';
import Perejil from '../images/perejil.png';
import Pimiento from '../images/pimiento.png';
import Rabanitos from '../images/rabanitos.png';
import Rúcula from '../images/rúcula.png';
import Zanahoria from '../images/zanahoria.png';
import Puerro from '../images/puerro.png';

const HortalizaImagen = ({ nombre }) => {
  const getHortalizaImage = (nombre) => {
    switch (nombre.toLowerCase()) {
      case 'tomate':
        return Tomate;
      case 'pepino':
        return Pepino;
      case 'ajo':
        return Ajo;
      case 'lechuga':
        return Lechuga;
      case 'tomate1':
        return Tomate1;
      case 'ajo1':
        return Ajo1;
      case 'lechuga1':
        return Lechuga1;
      case 'berenjena':
        return Berenjena;
      case 'calabaza':
        return Calabaza;
      case 'acelga':
        return Acelga;
      case 'fresa':
        return Fresa;
      case 'albahaca':
        return Albahaca;
      case 'alcachofa':
        return Alcachofa;
      case 'brócoli':
        return Brócoli;
      case 'calabacín':
        return Calabacín;
      case 'cebollino':
        return Cebollino;
      case 'coliflor':
        return Coliflor;
      case 'escarola':
        return Escarola;
      case 'melón':
        return Melón;
      case 'sandía':
        return Sandía;
      case 'maíz':
        return Maíz;
      case 'remolacha':
        return Remolacha;
      case 'patata':
        return Patata;
      case 'nabo':
        return Nabo;
      case 'apio':
        return Apio;
      case 'canónigo':
        return Canónigo;
      case 'cebollas':
        return Cebollas;
      case 'col':
        return Col;
      case 'espinacas':
        return Espinacas;
      case 'guisantes':
        return Guisantes;
      case 'habas':
        return Habas;
      case 'judías':
        return Judías;
      case 'perejil':
        return Perejil;
      case 'pimiento':
        return Pimiento;
      case 'rabanitos':
        return Rabanitos;
      case 'rúcula':
        return Rúcula;
      case 'zanahoria':
        return Zanahoria;
      case 'puerro':
        return Puerro;
      default:
        return Usuario;
    }
  };

  const imagen = getHortalizaImage(nombre);

  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <img
        src={imagen}
        alt={nombre}
        style={{
          objectFit: 'contain',
          width: '85%',
          height: '85%',
          display: 'block',
          margin: 'auto',
        }}
      />
    </div>
  );
};

export default HortalizaImagen;