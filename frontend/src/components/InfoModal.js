import React, { useState, useEffect } from 'react';
import {
  Modal,
  Backdrop,
  Fade,
  Paper,
  Typography,
  Button,
  makeStyles,
  Grid,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import axios from 'axios';

import {
  BugReport as BugIcon,
} from '@material-ui/icons';

import CancelIcon from '@material-ui/icons/Cancel';
import FlowerIcon from '@material-ui/icons/LocalFlorist';
import OpacityIcon from '@material-ui/icons/Opacity';
import { Eco as SaplingIcon } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: '#fafafa',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(6),
    textAlign: 'center',
    borderRadius: theme.shape.borderRadius * 2,
    maxHeight: '80vh',
    overflowY: 'auto',
    maxWidth: '75%',
    width: '50%',
    [theme.breakpoints.up('md')]: {
      maxWidth: '80%',
    },
  },
  title: {
    color: theme.palette.text.primary,
    marginBottom: theme.spacing(4),
    fontWeight: 'bold',
    borderBottom: `2px solid ${theme.palette.secondary.main}`,
    paddingBottom: theme.spacing(2),
  },
  content: {
    color: theme.palette.text.primary,
  },
  closeButton: {
    marginTop: theme.spacing(4),
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  deleteIcon: {
    color: theme.palette.error.main,
    marginLeft: theme.spacing(1),
    cursor: 'pointer',
  },
  contentContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
}));

const InfoModal = ({ open, handleClose, hortalizaInfo, siembraData, huertoIdModal, celdaIdModal }) => {
  const theme = useTheme();
  const classes = useStyles();
  const [riegosData, setRiegosData] = useState([]);
  const [plagasData, setPlagasData] = useState([]);
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  const parseAndFormatDate = (dateString) => {
    const options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
    return new Date(dateString).toLocaleString('es-ES', options);
  };

  const handleDeleteRiego = async (riegoId) => {
    try {
      const apiUrl = `${baseURL}/api/riegos/${riegoId}/delete`;
      await axios.delete(apiUrl, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });

      const updatedRiegos = riegosData.filter((riego) => riego.id !== riegoId);
      setRiegosData(updatedRiegos);
    } catch (error) {
      console.error('Error al eliminar el riego:', error);
    }
  };

  const handleDeletePlaga = async (plagaId) => {
    try {
      const apiUrl = `${baseURL}/api/plagas/${plagaId}/delete`;
      await axios.delete(apiUrl, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });

      const updatedPlagas = plagasData.filter((plaga) => plaga.id !== plagaId);
      setPlagasData(updatedPlagas);
    } catch (error) {
      console.error('Error al eliminar la plaga:', error);
    }
  };

  useEffect(() => {
    const fetchRiegosData = async () => {
      try {
        if (hortalizaInfo) {
          const apiUrl = `${baseURL}/api/riegos/`;
          const response = await axios.get(apiUrl, {
            params: {
              id_huerto: huertoIdModal,
              id_celda: celdaIdModal,
            },
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          });
          setRiegosData(response.data);
        }
      } catch (error) {
        console.error('Error al obtener datos de riegos:', error);
      }
    };
    
    const fetchPlagasData = async () => {
      try {
        if (hortalizaInfo) {
          const apiUrl = `${baseURL}/api/plagas/`;
          const response = await axios.get(apiUrl, {
            params: {
              id_huerto: huertoIdModal,
              id_celda: celdaIdModal,
            },
            headers: {
              'Authorization': `Token ${localStorage.getItem('token')}`,
            },
          });
          setPlagasData(response.data);
        }
      } catch (error) {
        console.error('Error al obtener datos de plagas:', error);
      }
    };

    if (hortalizaInfo) {
      fetchRiegosData();
      fetchPlagasData();
    }
  }, [open, hortalizaInfo]);

  return (
    <Modal
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Paper className={classes.paper}>
          <Typography variant="h4" className={classes.title}>
            Información Adicional
          </Typography>
          {hortalizaInfo ? (
            <>
              {hortalizaInfo.name && (
                <Typography variant="body1" className={classes.content}>
                  Hortaliza: {hortalizaInfo.name}
                </Typography>
                
              )}
              <Typography variant="body1" className={classes.content}>
                  <SaplingIcon style={{ verticalAlign: 'middle', color: 'green', marginRight: theme.spacing(1) }} />
                  Fecha de siembra: {parseAndFormatDate(siembraData.Fecha_siembra)}
                </Typography>
            </>
          ) : (
            <Typography variant="body1" className={classes.content}>
              No hay información de la hortaliza disponible.
            </Typography>
          )}
          <Grid container spacing={3} style={{ marginTop: theme.spacing(3) }}>
            <Grid item xs={4}>
              <Typography variant="h6" style={{ borderBottom: `2px solid black`, marginBottom: theme.spacing(1) }}>
                Fecha de cosecha
              </Typography>
              {siembraData && siembraData.Fecha_cos_real ? (
                <Typography variant="body1" className={classes.content}>
                  <FlowerIcon style={{ verticalAlign: 'middle', color: 'green', marginRight: theme.spacing(1) }} />
                  {parseAndFormatDate(siembraData.Fecha_cos_real)}
                </Typography>
              ) : (
                <Typography variant="body1" className={classes.content}>
                  No existe fecha de cosecha.
                </Typography>
              )}
            </Grid>
            <Grid item xs={4}>
              <Typography variant="h6" style={{ borderBottom: `2px solid black`, marginBottom: theme.spacing(1) }}>
                Riegos
              </Typography>
              {Array.isArray(riegosData) && riegosData.length > 0 ? (
                riegosData.map((riego) => (
                  <div key={riego.id} className={classes.contentContainer}>
                    <Typography variant="body1" className={classes.content}>
                      <OpacityIcon style={{ verticalAlign: 'middle', color: 'blue', marginRight: theme.spacing(1) }} />
                      {parseAndFormatDate(riego.fecha)}
                    </Typography>
                    <CancelIcon
                      className={classes.deleteIcon}
                      onClick={() => handleDeleteRiego(riego.id)}
                    />
                  </div>
                ))
              ) : (
                <Typography variant="body1" className={classes.content}>
                  No hay datos de riegos disponibles.
                </Typography>
              )}
            </Grid>
            <Grid item xs={4}>
              <Typography variant="h6" style={{ borderBottom: `2px solid black`, marginBottom: theme.spacing(1) }}>
                Plagas
              </Typography>
              {Array.isArray(plagasData) && plagasData.length > 0 ? (
                plagasData.map((plaga) => (
                  <div key={plaga.id} className={classes.contentContainer}>
                    <Typography variant="body1" className={classes.content}>
                      <BugIcon style={{ verticalAlign: 'middle', color: 'black', marginRight: theme.spacing(1) }} />
                      {parseAndFormatDate(plaga.fecha)}
                    </Typography>
                    <CancelIcon
                      className={classes.deleteIcon}
                      onClick={() => handleDeletePlaga(plaga.id)}
                    />
                  </div>
                ))
              ) : (
                <Typography variant="body1" className={classes.content}>
                  No hay datos de plagas disponibles.
                </Typography>
              )}
            </Grid>
          </Grid>
          <Button
            onClick={handleClose}
            variant="contained"
            className={classes.closeButton}
          >
            Cerrar
          </Button>
        </Paper>
      </Fade>
    </Modal>
  );
};

export default InfoModal;