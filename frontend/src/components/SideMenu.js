import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemIcon, ListItemText, Collapse } from '@material-ui/core';
import EcoIcon from '@material-ui/icons/Eco';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import EventIcon from '@material-ui/icons/Event';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';

const useStyles = makeStyles((theme) => ({
  list: {
    width: 250,
    position: 'fixed',
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const SideMenu = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [openHortalizas, setOpenHortalizas] = useState(false);
  const [openCalendario, setOpenCalendario] = useState(false);

  const handleHuertos = () => {
    navigate('/mis_huertos');
  };

  const handleHortalizas = () => {
    setOpenHortalizas(!openHortalizas);
  };

  const handleHortalizasPorFamilia = () => {
    navigate('/hortalizas_familia');
  };

  const handleMisHortalizas = () => {
    navigate('/mis_hortalizas');
  };

  const handleTodasHortalizas = () => {
    navigate('/hortalizas_sistema');
  };

  const handleEstadisticas = () => {
    navigate('/estadisticas');
  };

  const handleCalendario = () => {
    setOpenCalendario(!openCalendario);
  };

  const handleCalendarioHortalizas = () => {
    navigate('/calendario');
  };

  const handleCalendarioAnual = () => {
    navigate('/cal_meses');
  };

  return (
    <div className={classes.list}>
      <List>
        <ListItem button onClick={handleHuertos}>
          <ListItemIcon><HomeWorkIcon /></ListItemIcon>
          <ListItemText primary="Mis huertos" />
        </ListItem>
        <ListItem button onClick={handleHortalizas}>
          <ListItemIcon><EcoIcon /></ListItemIcon>
          <ListItemText primary="Hortalizas" />
          {openHortalizas ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openHortalizas} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested} onClick={handleTodasHortalizas}>
              <ListItemText primary="Todas las Hortalizas" />
            </ListItem>
            <ListItem button className={classes.nested} onClick={handleHortalizasPorFamilia}>
              <ListItemText primary="Hortalizas por Familia" />
            </ListItem>
            <ListItem button className={classes.nested} onClick={handleMisHortalizas}>
              <ListItemText primary="Mis Hortalizas" />
            </ListItem>
          </List>
        </Collapse>
        
        <ListItem button onClick={handleCalendario}>
          <ListItemIcon><EventIcon /></ListItemIcon>
          <ListItemText primary="Calendario de siembra" />
          {openCalendario ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openCalendario} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested} onClick={handleCalendarioHortalizas}>
              <ListItemIcon><DateRangeIcon /></ListItemIcon>
              <ListItemText primary="Calendario Hortalizas" />
            </ListItem>
            <ListItem button className={classes.nested} onClick={handleCalendarioAnual}>
              <ListItemIcon><EventAvailableIcon /></ListItemIcon>
              <ListItemText primary="Calendario Anual" />
            </ListItem>
          </List>
        </Collapse>

        <ListItem button onClick={handleEstadisticas}>
          <ListItemIcon><ShowChartIcon /></ListItemIcon>
          <ListItemText primary="Estadísticas" />
        </ListItem>
      </List>
    </div>
  );
};

export { SideMenu };