import React, { useEffect, useState } from 'react';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';
import {
  Card,
  CardActionArea,
  CardContent,
  Typography,
  makeStyles,
  Grid,
  Box,
} from '@material-ui/core';
import Header from './Header';
import { SideMenu } from './SideMenu';
import HortalizaImagen from './HortalizaImagen';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideMenu: {
    marginTop: theme.spacing(8),
  },
  content: {
    marginTop: theme.spacing(8),
    marginLeft: 300,
  },
  listItem: {
    marginBottom: theme.spacing(1),
  },
  card: {
    maxWidth: '90%',
    borderRadius: 16,
    overflow: 'hidden',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1), 0 8px 16px rgba(0, 0, 0, 0.1)',
    transition: 'transform 0.2s',
    '&:hover': {
      transform: 'scale(1.05)',
    },
  },
  mediaContainer: {
    height: 150,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  media: {
    objectFit: 'contain',
    width: '100%',
    height: '100%',
    transition: 'transform 0.2s',
    '&:hover': {
      transform: 'scale(1.1)',
    },
  },
  sectionTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(4),
    fontSize: 26,
  },
}));

function HortalizasSistema() {
  const classes = useStyles();
  const [hortalizasSistema, setHortalizasSistema] = useState([]);
  const { enqueueSnackbar } = useSnackbar();
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    fetch(`${baseURL}/api/hortalizas/sistema/`, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => setHortalizasSistema(data))
      .catch(error => enqueueSnackbar(`Error al obtener hortalizas del sistema: ${error.message}`, { variant: 'error' }));
  }, [enqueueSnackbar]);

  return (
    <div className={classes.root} style={{ overflowX: 'hidden', overflowY: 'hidden' }}>
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content}>
          <Typography variant="h4" className={classes.sectionTitle}>
            Hortalizas del Sistema
          </Typography>
          <Grid container spacing={2}>
            {hortalizasSistema.map((hortaliza) => (
              <Grid item xs={12} sm={6} md={4} lg={2} key={hortaliza.Id}>
                <Card className={classes.card}>
                  <Link to={`/hortalizas/${hortaliza.Id}`} style={{ textDecoration: 'none' }}>
                    <CardActionArea>
                      <div className={classes.mediaContainer}>
                        <HortalizaImagen nombre={hortaliza.Nombre} />
                      </div>
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          {hortaliza.Nombre}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                  </Link>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Box>
    </div>
  );
}

export default HortalizasSistema;