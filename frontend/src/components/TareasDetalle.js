import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
  Card,
  Typography,
  makeStyles,
  Button,
  TextField,
  Grid,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Box,
  Snackbar,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Header from './Header';
import { SideMenu } from './SideMenu';
import { useParams, useNavigate } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(2),
    marginLeft: 300,
    marginTop: theme.spacing(8),
  },
  addButton: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    gap: theme.spacing(2),
    alignItems: 'center',
  },
  taskCard: {
    marginBottom: theme.spacing(2),
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: theme.spacing(1),
  },
  formControl: {
    minWidth: 120,
    marginRight: theme.spacing(1),
  },
  sideMenu: {
    width: 250,
    marginTop: theme.spacing(8),
    backgroundColor: theme.palette.background.default,
    position: 'fixed',
    height: '100%',
    overflowY: 'auto',
    zIndex: theme.zIndex.drawer,
  },
  deleteButton: {
    backgroundColor: '#FF0000',
    color: '#FFFFFF',
    marginRight: theme.spacing(1),
  },
  editButton: {
    backgroundColor: '#000080',
    color: '#FFFFFF',
  },
}));

const TareasDetalle = () => {
  const classes = useStyles();
  const { id } = useParams();
  const navigate = useNavigate();
  const [tareas, setTareas] = useState([]);
  const [newTarea, setNewTarea] = useState('');
  const [newTareaFecha, setNewTareaFecha] = useState('');
  const [newTareaEstado, setNewTareaEstado] = useState('');
  const [editingItemId, setEditingItemId] = useState(null);
  const [editingTarea, setEditingTarea] = useState('');
  const [editingTareaFecha, setEditingTareaFecha] = useState('');
  const [editingTareaEstado, setEditingTareaEstado] = useState('');
  const [estadosTarea, setEstadosTarea] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('');
  const [deletedItemId, setDeletedItemId] = useState(null);
  const [confirmDeleteDialogOpen, setConfirmDeleteDialogOpen] = useState(false);
  const [deletingItemId, setDeletingItemId] = useState(null);
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const fetchData = async () => {
      try {
        const tareasResponse = await axios.get(`${baseURL}/api/huertos/${id}/tareas/`, {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        });

        const sortedTareas = tareasResponse.data.sort((a, b) => new Date(b.fecha) - new Date(a.fecha));
        setTareas(sortedTareas);

        const estadosTareaResponse = await axios.get(`${baseURL}/api/estado_tarea/`, {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        });
        setEstadosTarea(estadosTareaResponse.data);
      } catch (error) {
        console.error('Hubo un error al obtener los datos:', error);
      }
    };

    fetchData();
  }, [id]);

  const handleAddTarea = async () => {
    if (!newTarea || !newTareaFecha || !newTareaEstado) {
      setSnackbarMessage('Por favor, complete todos los campos.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
      return;
    }

    try {
      const response = await axios.post(
        `${baseURL}/api/huertos/${id}/tareas/create/`,
        {
          nota: newTarea,
          fecha: newTareaFecha,
          id_estado: newTareaEstado,
        },
        {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        }
      );

      const newTareaData = response.data;
      setNewTarea('');
      setNewTareaFecha('');
      setNewTareaEstado('');
      const updatedTareas = [...tareas, newTareaData].sort((a, b) => new Date(b.fecha) - new Date(a.fecha));
      setTareas(updatedTareas);
      setSnackbarMessage('Tarea agregada con éxito.');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);
    } catch (error) {
      console.error('Hubo un error al agregar una nueva tarea:', error);
      setSnackbarMessage('Hubo un error al agregar una nueva tarea.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
    }
  };

  const handleDeleteTarea = (tareaId) => {
    setDeletingItemId(tareaId);
    setConfirmDeleteDialogOpen(true);
  };

  const handleConfirmDelete = async () => {
    setConfirmDeleteDialogOpen(false);

    try {
      await axios.delete(`${baseURL}/api/tareas/${deletingItemId}/delete/`, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });

      setDeletedItemId(deletingItemId);
      setTareas((prevTareas) => prevTareas.filter((tarea) => tarea.id !== deletingItemId));
      setSnackbarMessage('Tarea eliminada correctamente.');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);
    } catch (error) {
      console.error('Hubo un error al eliminar la tarea:', error);
      setSnackbarMessage('Hubo un error al eliminar la tarea.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
    }
  };

  const handleCancelDelete = () => {
    setDeletingItemId(null);
    setConfirmDeleteDialogOpen(false);
  };

  const handleEditClick = (tarea) => {
    setEditingItemId(tarea.id);
    setEditingTarea(tarea.nota);
    setEditingTareaFecha(tarea.fecha);
    setEditingTareaEstado(tarea.id_estado);
  };

  const handleSaveChanges = async () => {
    try {
      await axios.put(
        `${baseURL}/api/tareas/${editingItemId}/update/`,
        {
          nota: editingTarea,
          fecha: new Date(editingTareaFecha).toISOString(),
          id_estado: editingTareaEstado,
        },
        {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`,
          },
        }
      );

      setEditingItemId(null);
      setSnackbarMessage('Cambios guardados con éxito.');
      setSnackbarSeverity('success');
      setSnackbarOpen(true);

      const updatedTareasResponse = await axios.get(`${baseURL}/api/huertos/${id}/tareas/`, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      });

      const sortedTareas = updatedTareasResponse.data.sort((a, b) => new Date(b.fecha) - new Date(a.fecha));
      setTareas(sortedTareas);
    } catch (error) {
      console.error('Hubo un error al actualizar la tarea:', error);
      setSnackbarMessage('Hubo un error al actualizar la tarea.');
      setSnackbarSeverity('error');
      setSnackbarOpen(true);
    }
  };

  const handleGoBack = () => {
    navigate(`/huerto/${id}/`);
  };

  const handleCloseSnackbar = () => {
    setSnackbarOpen(false);
  };

  const getBackgroundColor = (estadoDescripcion) => {
    switch (estadoDescripcion) {
      case 'Pendiente':
        return '#FFFFE0';
      case 'En progreso':
        return '#87CEEB';
      case 'Completada':
        return '#FFA07A';
      default:
        return '#FFFFFF';
    }
  };  
  
  return (
    <div className={classes.root}>
      <Header />
      <Box className={classes.sideMenu}>
        <SideMenu />
      </Box>
      <Box className={classes.content} flexGrow={1}>
        <Grid container spacing={2}>
          <Grid item xs={12} className={classes.addButton}>
            <TextField
              label="Nueva Tarea"
              variant="outlined"
              fullWidth
              value={newTarea}
              onChange={(e) => setNewTarea(e.target.value)}
              style={{ width: '250px' }}
            />
            <TextField
              label="Fecha"
              type="date"
              variant="outlined"
              fullWidth
              value={newTareaFecha}
              onChange={(e) => setNewTareaFecha(e.target.value)}
              InputLabelProps={{ shrink: true }}
              style={{ width: '200px', marginRight: '16px' }}
            />
            <FormControl className={classes.formControl} style={{ minWidth: '150px', marginRight: '16px' }}>
              <InputLabel>Estado de la Tarea</InputLabel>
              <Select
                value={newTareaEstado}
                onChange={(e) => setNewTareaEstado(e.target.value)}
              >
                {estadosTarea.map((estado) => (
                  <MenuItem key={estado.id} value={estado.id}>
                    {estado.descripcion}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAddTarea}
              >
                Añadir Tarea
              </Button>
              <Button
                variant="contained"
                color="default"
                onClick={handleGoBack}
                style={{ marginLeft: '8px' }}
              >
                Volver al Huerto
              </Button>
            </div>
          </Grid>
          {tareas.map((tarea) => (
            <Grid item xs={12} key={tarea.id}>
              <Card
                className={classes.taskCard}
                style={{ backgroundColor: getBackgroundColor(estadosTarea.find((estado) => estado.id === tarea.id_estado)?.descripcion) }}
              >
                <Box style={{ flex: '1' }}>
                  <Typography variant="body1" style={{ maxWidth: '200px' }}>
                    {tarea.nota}
                  </Typography>
                  <Typography variant="subtitle2">
                    Fecha: {new Date(tarea.fecha).toLocaleDateString()}
                  </Typography>
                  <Typography variant="subtitle2">
                    Estado: {estadosTarea.find((estado) => estado.id === tarea.id_estado)?.descripcion}
                  </Typography>
                </Box>
                {editingItemId === tarea.id ? (
                  <div></div>
                ) : (
                  <div>
                    <Button
                      variant="outlined"
                      className={classes.deleteButton}
                      onClick={() => handleDeleteTarea(tarea.id)}
                    >
                      Eliminar Tarea
                    </Button>
                    <Button
                      variant="outlined"
                      className={classes.editButton}
                      onClick={() => handleEditClick(tarea)}
                    >
                      Editar
                    </Button>
                  </div>
                )}
                {editingItemId === tarea.id && (
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <TextField
                      label="Nueva Tarea"
                      variant="outlined"
                      fullWidth
                      value={editingTarea}
                      onChange={(e) => setEditingTarea(e.target.value)}
                      style={{ width: '250px' }}
                    />
                    <TextField
                      label="Fecha"
                      type="date"
                      variant="outlined"
                      fullWidth
                      value={editingTareaFecha}
                      onChange={(e) => setEditingTareaFecha(e.target.value)}
                      InputLabelProps={{ shrink: true }}
                      style={{ width: '200px', marginLeft: '16px' }}
                    />
                    <FormControl className={classes.formControl} style={{ minWidth: '150px', marginLeft: '16px' }}>
                      <InputLabel>Estado de la Tarea</InputLabel>
                      <Select
                        value={editingTareaEstado}
                        onChange={(e) => setEditingTareaEstado(e.target.value)}
                      >
                        {estadosTarea.map((estado) => (
                          <MenuItem key={estado.id} value={estado.id}>
                            {estado.descripcion}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={handleSaveChanges}
                      style={{ marginLeft: '8px' }}
                    >
                      Grabar
                    </Button>
                  </div>
                )}
              </Card>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <MuiAlert onClose={handleCloseSnackbar} severity={snackbarSeverity}>
          {snackbarMessage}
        </MuiAlert>
      </Snackbar>
  
      {confirmDeleteDialogOpen && (
        <Dialog open={confirmDeleteDialogOpen} onClose={handleCancelDelete}>
          <DialogTitle>Confirmar Eliminación</DialogTitle>
          <DialogContent>
            <DialogContentText>
              ¿Está seguro de que desea eliminar esta tarea?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCancelDelete} color="primary">
              Cancelar
            </Button>
            <Button onClick={handleConfirmDelete} color="primary">
              Eliminar
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </div>
  );
};

export default TareasDetalle;