import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSnackbar } from 'notistack';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import PersonIcon from '@material-ui/icons/Person';
import Header from './Header';
import { SideMenu } from './SideMenu';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    height: '100vh',
  },
  sideMenu: {
    marginTop: theme.spacing(8),
    width: 250,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: theme.spacing(8),
    marginLeft: theme.spacing(6),
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(8, 4),
    maxWidth: '800px',
    width: '90%',
    marginLeft: theme.spacing(30),
    marginTop: theme.spacing(10),
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '60%',
    marginTop: theme.spacing(3),
  },
  formEditable: {
    width: '40%',
    marginTop: theme.spacing(3),
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(1),
  },
  paperContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

function Perfil() {
  const [userData, setUserData] = useState({
    username: '',
    first_name: '',
    last_name: '',
    email: '',
  });

  const [editMode, setEditMode] = useState(false);
  const [editedData, setEditedData] = useState({
    first_name: '',
    last_name: '',
    email: '',
  });

  const [changePasswordMode, setChangePasswordMode] = useState(true);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();
  const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'http://huertovirtual.site:8000';

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await axios.get(`${baseURL}/api/get_user_data/`, {
          headers: {
            'Authorization': `Token ${localStorage.getItem('token')}`
          }
        });
        setUserData(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  const handleEditModeToggle = () => {
    setEditMode(!editMode);
    setEditedData({
      first_name: userData.first_name,
      last_name: userData.last_name,
      email: userData.email,
    });
    setChangePasswordMode(false);
  };

  const handleFieldChange = (fieldName, value) => {
    setEditedData((prevData) => ({ ...prevData, [fieldName]: value }));
  };

  const handleSaveChanges = async () => {
    try {
      await axios.post(`${baseURL}/api/update_user_data/`, editedData, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`
        }
      });
      enqueueSnackbar('Datos actualizados con éxito', { variant: 'success' });
      setEditMode(false);
      setUserData((prevUserData) => ({ ...prevUserData, ...editedData }));
      setChangePasswordMode(true);
    } catch (error) {
      enqueueSnackbar('Error al actualizar datos. Por favor, inténtelo de nuevo.', {
        variant: 'error',
      });
    }
  };

  const handlePasswordChange = async () => {
    try {
      await axios.post(`${baseURL}/api/change_password/`, {
        old_password: oldPassword,
        new_password: newPassword,
      }, {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`
        }
      });
      enqueueSnackbar('Contraseña actualizada con éxito', { variant: 'success' });
      setOldPassword('');
      setNewPassword('');
      setChangePasswordMode(true);
    } catch (error) {
      enqueueSnackbar('Error al cambiar la contraseña. Por favor, inténtelo de nuevo.', {
        variant: 'error',
      });
    }
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Header />
      <Box display="flex">
        <Box className={classes.sideMenu}>
          <SideMenu />
        </Box>
        <Box className={classes.content}>
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <PersonIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Perfil de Usuario
            </Typography>
            {editMode ? (
              <form className={`${classes.form} ${classes.formEditable}`}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      label="Nombre de usuario"
                      value={userData.username}
                      disabled
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Nombre"
                      value={editedData.first_name}
                      onChange={(e) => handleFieldChange('first_name', e.target.value)}
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Apellido"
                      value={editedData.last_name}
                      onChange={(e) => handleFieldChange('last_name', e.target.value)}
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Correo electrónico"
                      value={editedData.email}
                      disabled
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                </Grid>
                <div className={classes.buttonContainer}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleSaveChanges}
                    className={classes.button}
                  >
                    Guardar Cambios
                  </Button>
                </div>
              </form>
            ) : (
              <>
                <TextField
                  label="Nombre de usuario"
                  value={userData.username}
                  disabled
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Nombre"
                  value={userData.first_name}
                  disabled
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Apellido"
                  value={userData.last_name}
                  disabled
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Correo electrónico"
                  value={userData.email}
                  disabled
                  fullWidth
                  margin="normal"
                />
                <div className={classes.buttonContainer}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleEditModeToggle}
                    className={classes.button}
                  >
                    Modificar Datos
                  </Button>
                </div>
              </>
            )}
          </Paper>
        </Box>
        <Box className={classes.content}>
          {changePasswordMode && (
            <Paper className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Cambiar Contraseña
              </Typography>
              <form className={classes.form}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <TextField
                      type="password"
                      label="Contraseña Actual"
                      value={oldPassword}
                      onChange={(e) => setOldPassword(e.target.value)}
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      type="password"
                      label="Nueva Contraseña"
                      value={newPassword}
                      onChange={(e) => setNewPassword(e.target.value)}
                      fullWidth
                      margin="normal"
                    />
                  </Grid>
                </Grid>
                <div className={classes.buttonContainer}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handlePasswordChange}
                    className={classes.button}
                  >
                    Cambiar Contraseña
                  </Button>
                </div>
              </form>
            </Paper>
          )}
        </Box>
      </Box>
    </div>
  );
}

export default Perfil;