from django.contrib import admin
from django.urls import path, re_path
from customers import views

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/cal_siembra/$', views.cal_siembra_list), #Calendario // #CalMeses
    re_path(r'^api/familias/$', views.familias_list), #CrearHortaliza  //  #HortalizasFamilia
    re_path(r'^api/tipo_riego/$', views.tipo_riego_list), #CrearHortaliza
    re_path(r'^api/hortalizas/$', views.hortalizas_list), #CrearHortaliza
    re_path(r'^api/user/(?P<pk>[0-9]+)$', views.user_detail),  #Dashboard  //  #Header
    re_path(r'api/estadisticas/', views.estadisticas),  #Estadisticas
    re_path(r'^api/huertos/(?P<pk>[0-9]+)/gastos/$', views.gastos_huerto),  #GastosHuerto
    re_path(r'^api/huertos/(?P<pk>[0-9]+)/gastos/create/$', views.gasto_create),  #GastosHuerto
    re_path(r'^api/gastos/(?P<pk>[0-9]+)/delete/$', views.gasto_delete),  #GastosHuerto
    re_path(r'^api/gastos/(?P<pk>[0-9]+)/update/$', views.update_gasto),  #GastosHuerto
    re_path(r'^api/hortalizas_v2/(?P<pk>[0-9]+)$', views.hortalizas_detail_v2),  #HortalizaDetalle
    re_path(r'^api/hortalizas_asociadas/(?P<Id_hortaliza>[0-9]+)$', views.hortalizas_asociadas),   #HortalizaDetalle
    re_path(r'^api/hortalizas_no_asociadas/(?P<Id_hortaliza>[0-9]+)$', views.hortalizas_no_asociadas),  #HortalizaDetalle
    re_path(r'^api/cal_siembra_hortaliza/(?P<Id_hortaliza>[0-9]+)$', views.cal_siembra_hortaliza, name='cal_siembra_hortaliza'),  #HortalizaDetalle
    re_path(r'api/hortalizas/familia/(?P<familia_id>[0-9]+)/', views.hortalizas_por_familia),   #HortalizasFamilia
    re_path(r'api/hortalizas/sistema/', views.hortalizas_sistema),   #HortalizasSistema   //  #HuertoDetalle
    re_path(r'^api/riegos/$', views.riegos_list),  #InfoModal   //  #HuertoDetalle
    re_path(r'^api/plagas/$', views.plagas_list),  #InfoModal   //  #HuertoDetalle
    re_path(r'^api/riegos/(?P<pk>[0-9]+)/delete$', views.delete_riego),   #InfoModal
    re_path(r'^api/plagas/(?P<pk>[0-9]+)/delete$', views.delete_plaga),   #InfoModal
    re_path(r'api/login/', views.login),  #Login
    re_path(r'api/hortalizas/mis_hortalizas/', views.mis_hortalizas),  #MisHortalizas   //  #HuertoDetalle
    re_path(r'^api/huertos/(?P<pk>[0-9]+)$', views.huertos_detail),   #MisHuertos   //  #HuertoDetalle
    re_path(r'api/mis_huertos/', views.mis_huertos, name='mis_huertos'),    #MisHuertos
    re_path(r'api/crear_huerto/', views.crear_huerto),    #MisHuertos
    re_path(r'api/check_huerto_name/', views.check_huerto_name),   #MisHuertos
    re_path(r'api/change_password/', views.change_password, name='change_password'),    #Perfil
    re_path(r'api/get_user_data/', views.get_user_data),    #Perfil
    re_path(r'api/update_user_data/', views.update_user_data),    #Perfil
    re_path(r'api/register/', views.register),   #Register
    re_path(r'^api/huertos/(?P<pk>[0-9]+)/tareas/$', views.tareas_huerto),   #TareasDetalle
    re_path(r'api/estado_tarea/', views.estado_tarea_list),    #TareasDetalle
    re_path(r'^api/huertos/(?P<pk>[0-9]+)/tareas/create/$', views.tarea_create),    #TareasDetalle
    re_path(r'^api/tareas/(?P<pk>[0-9]+)/delete/$', views.tarea_delete),    #TareasDetalle
    re_path(r'^api/tareas/(?P<pk>[0-9]+)/update/$', views.update_tarea),    #TareasDetalle
    re_path(r'api/huertos/(?P<pk>[0-9]+)/inventario/', views.inventario_huerto),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/siembras$', views.create_siembra),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/siembras/delete$', views.delete_siembra),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/siembras/manage$', views.manage_siembra),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/siembras/update$', views.update_siembra),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/get_siembras$', views.get_siembra),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/plagas/create$', views.create_plaga),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/celdas/(?P<celda_id>[A-Z][0-9]+)/riegos/create$', views.create_riego),  #HuertoDetalle
    re_path(r'^api/huertos/(?P<huerto_id>[0-9]+)/siembras$', views.get_siembras),   #HuertoDetalle
]